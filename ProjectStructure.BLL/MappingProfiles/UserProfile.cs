﻿using AutoMapper;
using ProjectStructure.Common;
using ProjectStructure.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace ProjectStructure.BLL.MappingProfiles
{
    public class UserProfile : Profile
    {
        public UserProfile()
        {
            CreateMap<UserDTO, User>()
            .ForMember(e => e.LastName, cfg => cfg.MapFrom(dto => dto.Last_name))
            .ForMember(e => e.FirstName, cfg => cfg.MapFrom(dto => dto.First_name))
            .ForMember(e => e.TeamId, cfg => cfg.MapFrom(dto => dto.Team_Id))
            .ForMember(e => e.RegisteredAt, cfg => cfg.MapFrom(dto => dto.Registered_at))
            .ReverseMap();

            CreateMap<CreateUserDTO, User>();
        }
    }
}
