﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ProjectStructure.BLL.Interfaces
{
    public interface IServiceAsyncWithMapping<TEntity, TDTO, TCreateDTO> : IServiceAsync<TEntity>, IMessageToQueue where TEntity : class
    {
         Task<TEntity> CreateAsyncMapped(TCreateDTO entity);
         Task<TDTO> GetAsyncMapped(int id);
         Task<IEnumerable<TDTO>> GetAllAsyncMapped();
         Task UpdateAsyncMapped(TDTO entity);
    }
}
