﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProjectStructure.BLL.Interfaces
{
   public interface IQueueService
    {
        void PushMessage(string message);
    }
}
