﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProjectStructure.BLL.Interfaces
{
    public interface IMessageToQueue
    {
        void SendMessageToQueue(string m);
    }
}
