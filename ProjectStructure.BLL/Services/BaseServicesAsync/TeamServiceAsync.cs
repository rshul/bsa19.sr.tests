﻿using ProjectStructure.BLL.Interfaces;
using ProjectStructure.DAL.Entities;
using ProjectStructure.DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ProjectStructure.BLL.Services.BaseServicesAsync
{
    public class TeamServiceAsync : IServiceAsync<Team>
    {
        protected readonly IUnitOfWorkAsync _repo;

        public TeamServiceAsync(IUnitOfWorkAsync unitOfWork)
        {
            _repo = unitOfWork;
        }

        public async Task<Team> CreateAsync(Team entity)
        {
            var task =  await _repo.TeamRepository.InsertAsync(entity);
            await SaveChangesAsync();
            return task;
        }

        public async Task DeleteAsync(int id)
        {
            await _repo.TeamRepository.DeleteAsync(id);
            await SaveChangesAsync();
        }

        public async Task<Team> GetAsync(int id)
        {
            return await _repo.TeamRepository.GetByIdAsync(id);
        }

        public async Task<IEnumerable<Team>> GetAllAsync()
        {
            return await _repo.TeamRepository.GetAllAsync();
        }

        public async Task UpdateAsync(Team entity)
        {
            await _repo.TeamRepository.UpdateAsync(entity.Id, entity);
            await SaveChangesAsync();
        }

        public async Task<int> SaveChangesAsync()
        {
            return await _repo.SaveChangesAsync();
        }

        public async Task<bool> IsEntityPresentAsync(int id)
        {
            var result = await GetAsync(id);
            return result != null;
        }
    }
}
