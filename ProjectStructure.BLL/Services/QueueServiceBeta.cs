﻿using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.Configuration;
using ProjectStructure.BLL.Hubs;
using ProjectStructure.BLL.Interfaces;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System;
using System.Collections.Generic;
using System.Text;

namespace ProjectStructure.BLL.Services
{
    public class QueueServiceBeta : IDisposable, IQueueService
    {
        private readonly IConfiguration _config;
        private IConnection _connectionProducer;
        private IModel _channelProducer;
        private IConnection _connectionConsumer;
        private IModel _channelConsumer;
        private EventingBasicConsumer _consumer;
        private IHubContext<MessageHub> _hub;
        public QueueServiceBeta(IConfiguration config, IHubContext<MessageHub> hub)
        {
            _config = config;
            _hub = hub;
            ConfigureConnection();

        }

        private void ConfigureConnection()
        {
            var factory = new ConnectionFactory()
            {
                HostName = "localhost"
            };

            _connectionProducer = factory.CreateConnection();
            _connectionConsumer = factory.CreateConnection();
            _channelProducer = _connectionProducer.CreateModel();
            _channelConsumer = _connectionConsumer.CreateModel();

            _channelProducer.ExchangeDeclare(exchange: "ToWorker", type: ExchangeType.Direct);

            _channelConsumer.ExchangeDeclare(exchange: "FromWorker", type: ExchangeType.Direct);
            _channelConsumer.QueueDeclare(
                queue: "server1",
                exclusive: false,
                durable: true,
                autoDelete: false);

            _channelConsumer.QueueBind(
                exchange: "FromWorker",
                queue:"server1",
                routingKey:"s1");

            _consumer = new EventingBasicConsumer(_channelConsumer);
            _consumer.Received += MessageReceived;
            _channelConsumer.BasicConsume(
                queue: "server1",
                autoAck: false,
                consumer: _consumer);

        }

        private void MessageReceived(object sender, BasicDeliverEventArgs e)
        {
            var body = e.Body;
            var message = Encoding.UTF8.GetString(body);
            Console.WriteLine($"Server message received {message}");
            _hub.Clients.All.SendAsync("NewMessage", message);

            _channelConsumer.BasicAck(deliveryTag: e.DeliveryTag, multiple:false);
        }

        public void PushMessage(string message)
        {   
            
                var body = Encoding.UTF8.GetBytes(message);
                _channelProducer.BasicPublish(
                    exchange: "ToWorker",
                    routingKey: "w1",
                    basicProperties: null,
                    body: body);   
        }

        public void Dispose()
        {
            _connectionProducer?.Dispose();
            _channelProducer?.Dispose();
            _connectionConsumer?.Dispose();
            _channelConsumer?.Dispose();
        }

    }
}
