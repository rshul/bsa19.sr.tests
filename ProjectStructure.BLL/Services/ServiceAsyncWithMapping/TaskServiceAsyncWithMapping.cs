﻿using AutoMapper;
using ProjectStructure.BLL.Interfaces;
using ProjectStructure.Common;
using ProjectStructure.DAL.Entities;
using ProjectStructure.DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ProjectStructure.BLL.Services
{
    public class TaskServiceAsyncWithMapping : TaskServiceAscync, IServiceAsyncWithMapping<ProjectTask, ProjectTaskDTO, CreateProjectTaskDTO>
    {
        private readonly IMapper _mapper;
        private readonly IQueueService _qs;

        public TaskServiceAsyncWithMapping(IUnitOfWorkAsync repo, IMapper mapper, IQueueService qs) : base(repo)
        {
            _mapper = mapper;
            _qs = qs;
        }

        public async Task<ProjectTask> CreateAsyncMapped(CreateProjectTaskDTO entity)
        {
            var taskFromDTO = _mapper.Map<ProjectTask>(entity);
            return await CreateAsync(taskFromDTO);
        }

        public async Task<IEnumerable<ProjectTaskDTO>> GetAllAsyncMapped()
        {
            var tasks = await GetAllAsync();
            return _mapper.Map<IEnumerable<ProjectTaskDTO>>(tasks);
        }

        public async Task<ProjectTaskDTO> GetAsyncMapped(int id)
        {
            var tasks = await GetAsync(id);
            return _mapper.Map<ProjectTaskDTO>(tasks);
        }

        public void SendMessageToQueue(string m)
        {
            _qs.PushMessage(m);
        }

        public async Task UpdateAsyncMapped(ProjectTaskDTO entity)
        {
            var canUpdate = await IsEntityPresentAsync(entity.Id);
            if (canUpdate)
            {
                var projectFromDTO = _mapper.Map<ProjectTask>(entity);
                await UpdateAsync(projectFromDTO);
            }
            else
            {
                throw new Exception("Not found entity");
            }
        }
    }
}
