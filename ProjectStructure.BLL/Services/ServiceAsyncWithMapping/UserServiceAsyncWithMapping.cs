﻿using AutoMapper;
using ProjectStructure.BLL.Interfaces;
using ProjectStructure.BLL.Services.BaseServicesAsync;
using ProjectStructure.Common;
using ProjectStructure.DAL.Entities;
using ProjectStructure.DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ProjectStructure.BLL.Services
{
   public class UserServiceAsyncWithMapping : UserServiceAsync, IServiceAsyncWithMapping<User, UserDTO, CreateUserDTO>
    {
        private readonly IMapper _mapper;
        private readonly IQueueService _qs;

        public UserServiceAsyncWithMapping(IUnitOfWorkAsync unitOfWork, IMapper mapper, IQueueService qs) : base(unitOfWork)
        {
            _mapper = mapper;
            _qs = qs;
        }

        public async Task<User> CreateAsyncMapped(CreateUserDTO entity)
        {
            var userFromDTO = _mapper.Map<User>(entity);
            return await CreateAsync(userFromDTO);
        }

        public async Task<UserDTO> GetAsyncMapped(int id)
        {
            var user = await GetAsync(id);
            return _mapper.Map<UserDTO>(user);
        }

        public async Task<IEnumerable<UserDTO>> GetAllAsyncMapped()
        {
            var users = await GetAllAsync();
            return _mapper.Map<IEnumerable<UserDTO>>(users);
        }

        public async Task UpdateAsyncMapped(UserDTO entity)
        {
            var canUpdate = await IsEntityPresentAsync(entity.Id);
            if (canUpdate)
            {
                var userFromDTO = _mapper.Map<User>(entity);
                await UpdateAsync(userFromDTO);
            }
            else
            {
                throw new Exception("Not found entity");
            }
        }

        public void SendMessageToQueue(string m)
        {
            _qs.PushMessage(m);
        }
    }
}
