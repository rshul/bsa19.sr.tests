﻿using AutoMapper;
using ProjectStructure.BLL.Interfaces;
using ProjectStructure.BLL.Services.BaseServicesAsync;
using ProjectStructure.Common;
using ProjectStructure.DAL.Entities;
using ProjectStructure.DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ProjectStructure.BLL.Services
{
    public class TaskStateServiceAsyncWithMapping: TaskStateServiceAsync, IServiceAsyncWithMapping<TaskState,TaskStateDTO,CreateTaskStateDTO>
    {
        private readonly IMapper _mapper;
        private readonly IQueueService _qs;

        public TaskStateServiceAsyncWithMapping(IUnitOfWorkAsync unitOfWork, IMapper mapper, IQueueService qs) : base(unitOfWork)
        {
            _mapper = mapper;
            _qs = qs;
        }

        public async Task<TaskState> CreateAsyncMapped(CreateTaskStateDTO entity)
        {
            var taskStateFromDTO = _mapper.Map<TaskState>(entity);
            return await CreateAsync(taskStateFromDTO);
        }

        public async Task<TaskStateDTO> GetAsyncMapped(int id)
        {
            var taskState = await GetAsync(id);
            return _mapper.Map<TaskStateDTO>(taskState);
        }

        public async Task<IEnumerable<TaskStateDTO>> GetAllAsyncMapped()
        {
            var taskStates = await GetAllAsync();
            return _mapper.Map<IEnumerable<TaskStateDTO>>(taskStates);
        }

        public async Task UpdateAsyncMapped(TaskStateDTO entity)
        {
            var canUpdate = await IsEntityPresentAsync(entity.Id);
            if (canUpdate)
            {
                var taskStateFromDTO = _mapper.Map<TaskState>(entity);
                await UpdateAsync(taskStateFromDTO);
            }
            else
            {
                throw new Exception("Not found entity");
            }
        }

        public void SendMessageToQueue(string m)
        {
            _qs.PushMessage(m);
        }
    }
}
