﻿using ProjectStructure.BLL.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace ProjectsTasks.Tests.Fake
{
    public class FakeQueService : IQueueService
    {
        public void PushMessage(string message)
        {
            Console.WriteLine($"To queue: {message }");
        }
    }
}
