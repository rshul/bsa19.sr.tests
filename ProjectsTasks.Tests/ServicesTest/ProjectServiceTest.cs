﻿using AutoMapper;
using AutoMapper.Configuration;
using NUnit.Framework;
using ProjectsTasks.Tests.Fake;
using ProjectStructure.BLL.Interfaces;
using ProjectStructure.BLL.MappingProfiles;
using ProjectStructure.BLL.Services;
using ProjectStructure.Common;
using ProjectStructure.DAL.Entities;
using ProjectStructure.DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace ProjectsTasks.Tests.ServicesTest
{
    [TestFixture]
    public class ProjectServiceTest
    {
        private readonly IUnitOfWorkAsync _fakeUow;
        private readonly IRepositoryAsync<Project> _fakeRepo;
        private readonly IMapper _mapper;
        private readonly IQueueService _fakeQService;
        private readonly IServiceAsyncWithMapping<Project, ProjectDTO,CreatProjectDTO> _projectService;

        public ProjectServiceTest()
        {
            var config = new MapperConfiguration(cfg => {
                cfg.AddProfile<ProjectProfile>();
            });
            var mapper = config.CreateMapper();
            _mapper = mapper;

            _fakeUow = new FakeUnitOfWork(new FakeContext());
            _fakeRepo = _fakeUow.ProjectRepository;
            _fakeQService = new FakeQueService();
           // _projectService = new ProjectServiceAsyncWithMapping(_fakeUow, mapper, new FakeQueService() );

        }
        [SetUp]
        public void Setup()
        {
            var createDto = new CreatProjectDTO()
            {
                Author_Id = 2,
                Created_At = new DateTime(2017, 2, 19, 21, 53, 20, 150, DateTimeKind.Local).AddTicks(8055),
                Deadline = new DateTime(2020, 9, 19, 12, 36, 3, 3, DateTimeKind.Local).AddTicks(1465),
                Description = @"Laborum nihil atque itaque ea sed quisquam dicta et eum.
Quisquam maxime consequatur quaerat autem in.",
                Name = "autem",
                Team_Id = 1
            };

            var project = new Project()
            {
                Id = 0,
                AuthorId = 3,
                CreatedAt = new DateTime(2016, 8, 17, 19, 34, 47, 242, DateTimeKind.Local).AddTicks(4345),
                Deadline = new DateTime(2021, 3, 23, 9, 59, 33, 146, DateTimeKind.Local).AddTicks(9089),
                Description = @"Veritatis ad culpa odit ea porro expedita.
Nostrum fuga at sed deserunt in ipsam.
Ut pariatur atque laudantium non non est.",
                Name = "est",
                TeamId = 2
            };
            (_fakeRepo as FakeGenericRepository<Project>)._repo.Add(project);

        }

        [TearDown]
        public  void TestTearDown()
        {
            (_fakeRepo as FakeGenericRepository<Project>)._repo.Clear();
        }

        public void GetById_When_ID_0()
        {
            //Arrange
            var projectService = new ProjectServiceAsyncWithMapping(_fakeUow, _mapper, _fakeQService);
            //Act
            var project = projectService.GetAsync(0);
            //Assert
            Assert.That(project.Id, Is.EqualTo(0));
        }
    }
}
