using System;
using ProjectStructure.DAL.Entities;

namespace ProjectStructure.DAL.Interfaces
{
    public interface IUnitOfWorkAsync : IDisposable, IChangesTrackableAsync
    {
        IRepositoryAsync<Project> ProjectRepository { get; }
        IRepositoryAsync<User> UserRepository { get; }
        IRepositoryAsync<Team> TeamRepository { get; }
        IRepositoryAsync<TaskState> TaskStateRepository { get; }
        IRepositoryAsync<ProjectTask> ProjectTaskRepository { get; }
    }
}