﻿using ProjectStructure.Common.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace ProjectStructure.DAL.Entities
{
    public class User : IEntity
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public DateTime Birthday { get; set; }
        public DateTime RegisteredAt { get; set; }

        public int? TeamId { get; set; }
        public Team Team { get; set; }

        public Project Project { get; set; }

        public ProjectTask Task { get; set; }
    }
}
