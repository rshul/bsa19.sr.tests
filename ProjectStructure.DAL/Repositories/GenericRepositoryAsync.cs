using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using ProjectStructure.Common.Interfaces;
using ProjectStructure.DAL.Interfaces;
using ProjectStructure.DAL.Context;


namespace ProjectStructure.DAL.Repositories
{
    public class GenericRepositoryAsync<TEntity> : IRepositoryAsync<TEntity> where TEntity : class
    {
        private readonly DbSet<TEntity> _dbSet;
        private readonly ProjectDbContext _context;

        public GenericRepositoryAsync(ProjectDbContext context)
        {
            _context = context;
            _dbSet = context.Set<TEntity>();
        }
        public async Task DeleteAsync(int id)
        {
            TEntity entity = await _dbSet.FindAsync(id);
            _dbSet.Remove(entity);
        }

        public async Task<System.Collections.Generic.IEnumerable<TEntity>> GetAllAsync()
        {
            return await _dbSet.ToListAsync();
        }

        public async Task<TEntity> GetByIdAsync(int id)
        {
            return await _dbSet.FindAsync(id);
        }

        public async Task<TEntity> InsertAsync(TEntity entity)
        {
            var result = await _dbSet.AddAsync(entity);
            return result.Entity;
        }

        public async Task UpdateAsync(int id, TEntity entity)
        {
            var found = await _dbSet.FindAsync(id);
            _context.Entry(found).CurrentValues.SetValues(entity);
            
        }
    }
}