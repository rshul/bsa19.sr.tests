﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace ProjectStructure.DAL.Migrations
{
    public partial class newSeeding : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "TaskStates",
                columns: new[] { "Id", "Value" },
                values: new object[,]
                {
                    { 1, "Created" },
                    { 2, "Started" },
                    { 3, "Finished" },
                    { 4, "Canceled" }
                });

            migrationBuilder.InsertData(
                table: "Teams",
                columns: new[] { "Id", "CreationDate", "TeamName" },
                values: new object[,]
                {
                    { 1, new DateTime(2019, 7, 15, 21, 2, 14, 784, DateTimeKind.Local).AddTicks(7685), "iure" },
                    { 2, new DateTime(2019, 7, 15, 22, 26, 1, 153, DateTimeKind.Local).AddTicks(9200), "deleniti" },
                    { 3, new DateTime(2019, 7, 15, 11, 2, 56, 606, DateTimeKind.Local).AddTicks(7914), "delectus" },
                    { 4, new DateTime(2019, 7, 15, 17, 27, 2, 802, DateTimeKind.Local).AddTicks(6632), "tempora" },
                    { 5, new DateTime(2019, 7, 15, 2, 37, 37, 495, DateTimeKind.Local).AddTicks(9201), "quae" }
                });

            migrationBuilder.InsertData(
                table: "Users",
                columns: new[] { "Id", "Birthday", "Email", "FirstName", "LastName", "RegistrationDate", "TeamId" },
                values: new object[,]
                {
                    { 2, new DateTime(1999, 4, 23, 5, 17, 53, 207, DateTimeKind.Local).AddTicks(5182), "Bessie0@gmail.com", "Garret", "Kilback", new DateTime(2019, 7, 5, 11, 20, 45, 697, DateTimeKind.Local).AddTicks(5256), 1 },
                    { 4, new DateTime(2007, 2, 8, 20, 1, 17, 440, DateTimeKind.Local).AddTicks(9208), "Yesenia17@yahoo.com", "Hollis", "Jenkins", new DateTime(2019, 6, 17, 12, 59, 56, 86, DateTimeKind.Local).AddTicks(6679), 4 },
                    { 19, new DateTime(1997, 4, 24, 2, 12, 23, 6, DateTimeKind.Local).AddTicks(5214), "Jay_Feil@hotmail.com", "Rosa", "Sanford", new DateTime(2019, 7, 11, 18, 44, 50, 979, DateTimeKind.Local).AddTicks(3548), 4 },
                    { 26, new DateTime(2008, 12, 24, 21, 54, 13, 744, DateTimeKind.Local).AddTicks(9102), "Ernestina.Ebert@hotmail.com", "Serenity", "Wiza", new DateTime(2019, 7, 6, 1, 51, 37, 703, DateTimeKind.Local).AddTicks(4256), 4 },
                    { 31, new DateTime(1990, 8, 23, 17, 24, 53, 65, DateTimeKind.Local).AddTicks(2183), "Elisa.Schimmel7@gmail.com", "Manuela", "Satterfield", new DateTime(2019, 7, 15, 4, 29, 11, 162, DateTimeKind.Local).AddTicks(1018), 4 },
                    { 33, new DateTime(1982, 4, 22, 6, 37, 9, 609, DateTimeKind.Local).AddTicks(4079), "Dan94@hotmail.com", "Domenico", "Runte", new DateTime(2019, 7, 11, 18, 42, 33, 481, DateTimeKind.Local).AddTicks(8230), 4 },
                    { 38, new DateTime(2002, 12, 15, 5, 16, 30, 901, DateTimeKind.Local).AddTicks(9062), "Freddy_Kulas5@yahoo.com", "Elta", "Block", new DateTime(2019, 7, 5, 22, 23, 45, 106, DateTimeKind.Local).AddTicks(5146), 4 },
                    { 41, new DateTime(1981, 9, 10, 18, 12, 7, 130, DateTimeKind.Local).AddTicks(1720), "Lolita_Armstrong@yahoo.com", "Oliver", "Thiel", new DateTime(2019, 7, 12, 5, 41, 37, 369, DateTimeKind.Local).AddTicks(3269), 4 },
                    { 44, new DateTime(1990, 7, 22, 13, 5, 49, 447, DateTimeKind.Local).AddTicks(8780), "Ellie89@gmail.com", "Adrienne", "Cormier", new DateTime(2019, 5, 2, 8, 1, 34, 990, DateTimeKind.Local).AddTicks(146), 4 },
                    { 46, new DateTime(1999, 9, 14, 16, 0, 31, 137, DateTimeKind.Local).AddTicks(9638), "Annabell.Metz95@yahoo.com", "Elda", "Gerlach", new DateTime(2019, 5, 25, 16, 31, 47, 536, DateTimeKind.Local).AddTicks(6398), 4 },
                    { 48, new DateTime(1993, 4, 21, 3, 0, 57, 552, DateTimeKind.Local).AddTicks(3351), "Kirk93@gmail.com", "Berniece", "Gerhold", new DateTime(2019, 5, 20, 2, 1, 41, 637, DateTimeKind.Local).AddTicks(8062), 4 },
                    { 1, new DateTime(1970, 9, 15, 23, 59, 38, 91, DateTimeKind.Local).AddTicks(4909), "Ewell46@gmail.com", "Ali", "McCullough", new DateTime(2019, 7, 14, 10, 15, 45, 663, DateTimeKind.Local).AddTicks(1594), 5 },
                    { 5, new DateTime(2011, 8, 17, 18, 52, 1, 582, DateTimeKind.Local).AddTicks(5610), "Dena_Farrell33@gmail.com", "Ernesto", "Bergstrom", new DateTime(2019, 5, 25, 15, 36, 52, 466, DateTimeKind.Local).AddTicks(3470), 5 },
                    { 6, new DateTime(2011, 8, 11, 12, 45, 23, 833, DateTimeKind.Local).AddTicks(5458), "Juvenal.Rowe@yahoo.com", "Buck", "Mann", new DateTime(2019, 7, 15, 4, 2, 49, 3, DateTimeKind.Local).AddTicks(2814), 5 },
                    { 8, new DateTime(2017, 6, 7, 2, 20, 1, 175, DateTimeKind.Local).AddTicks(647), "Arely56@yahoo.com", "Otha", "Hermann", new DateTime(2019, 6, 18, 23, 30, 37, 395, DateTimeKind.Local).AddTicks(6703), 5 },
                    { 9, new DateTime(2010, 11, 11, 8, 37, 52, 40, DateTimeKind.Local).AddTicks(1374), "Shanelle_Wuckert49@yahoo.com", "Raphael", "Koch", new DateTime(2019, 6, 7, 16, 38, 6, 564, DateTimeKind.Local).AddTicks(3274), 5 },
                    { 12, new DateTime(2016, 7, 15, 15, 26, 5, 686, DateTimeKind.Local).AddTicks(3863), "Neil_DAmore31@hotmail.com", "Randi", "Sanford", new DateTime(2019, 6, 29, 21, 21, 20, 913, DateTimeKind.Local).AddTicks(8177), 5 },
                    { 24, new DateTime(2004, 3, 4, 2, 9, 33, 572, DateTimeKind.Local).AddTicks(7806), "Creola_Tillman@hotmail.com", "Monique", "Wilkinson", new DateTime(2019, 6, 15, 14, 59, 28, 176, DateTimeKind.Local).AddTicks(9654), 5 },
                    { 28, new DateTime(2018, 7, 30, 1, 48, 4, 544, DateTimeKind.Local).AddTicks(1785), "Kevon.Dietrich@hotmail.com", "Jayme", "Kozey", new DateTime(2019, 6, 23, 21, 51, 29, 624, DateTimeKind.Local).AddTicks(4664), 5 },
                    { 32, new DateTime(2014, 4, 2, 4, 4, 28, 318, DateTimeKind.Local).AddTicks(4217), "Birdie33@gmail.com", "Darrin", "Gislason", new DateTime(2019, 6, 14, 6, 34, 42, 961, DateTimeKind.Local).AddTicks(6256), 5 },
                    { 36, new DateTime(2017, 6, 21, 9, 35, 18, 923, DateTimeKind.Local).AddTicks(9423), "Leilani.Shanahan@hotmail.com", "Belle", "Conroy", new DateTime(2019, 5, 6, 6, 15, 17, 277, DateTimeKind.Local).AddTicks(3881), 5 },
                    { 37, new DateTime(2000, 12, 2, 23, 10, 48, 893, DateTimeKind.Local).AddTicks(1044), "Lily_Wolf@yahoo.com", "Brielle", "Williamson", new DateTime(2019, 5, 19, 23, 27, 27, 421, DateTimeKind.Local).AddTicks(6675), 5 },
                    { 49, new DateTime(2012, 1, 22, 18, 10, 17, 378, DateTimeKind.Local).AddTicks(1475), "Murphy_Zemlak@hotmail.com", "Jamil", "Windler", new DateTime(2019, 5, 16, 11, 29, 12, 284, DateTimeKind.Local).AddTicks(9303), 3 },
                    { 30, new DateTime(2011, 11, 8, 0, 44, 22, 95, DateTimeKind.Local).AddTicks(9866), "Zackery.Sanford81@yahoo.com", "Dulce", "Lind", new DateTime(2019, 5, 27, 23, 31, 5, 624, DateTimeKind.Local).AddTicks(8016), 3 },
                    { 25, new DateTime(2006, 9, 14, 22, 25, 57, 513, DateTimeKind.Local).AddTicks(3620), "Simone99@gmail.com", "Lincoln", "Deckow", new DateTime(2019, 7, 3, 4, 3, 29, 182, DateTimeKind.Local).AddTicks(7668), 3 },
                    { 23, new DateTime(2012, 6, 16, 11, 45, 31, 571, DateTimeKind.Local).AddTicks(8966), "Joanne.Johns85@hotmail.com", "Maureen", "Parker", new DateTime(2019, 5, 25, 5, 58, 4, 101, DateTimeKind.Local).AddTicks(8642), 3 },
                    { 14, new DateTime(2019, 5, 24, 6, 11, 24, 208, DateTimeKind.Local).AddTicks(8968), "Ransom3@yahoo.com", "Hermina", "Weissnat", new DateTime(2019, 5, 26, 23, 38, 15, 114, DateTimeKind.Local).AddTicks(9488), 1 },
                    { 20, new DateTime(1994, 4, 16, 3, 13, 31, 644, DateTimeKind.Local).AddTicks(3781), "Clifford.Schulist@hotmail.com", "Alice", "Murazik", new DateTime(2019, 6, 22, 21, 21, 46, 310, DateTimeKind.Local).AddTicks(2039), 1 },
                    { 21, new DateTime(2017, 5, 28, 8, 57, 13, 384, DateTimeKind.Local).AddTicks(7855), "Ayla.Wintheiser67@yahoo.com", "Carole", "Muller", new DateTime(2019, 6, 9, 6, 2, 49, 441, DateTimeKind.Local).AddTicks(3780), 1 },
                    { 22, new DateTime(2003, 11, 30, 5, 44, 13, 156, DateTimeKind.Local).AddTicks(4816), "Ashley4@gmail.com", "Ericka", "Pfannerstill", new DateTime(2019, 5, 23, 8, 15, 52, 225, DateTimeKind.Local).AddTicks(7413), 1 },
                    { 27, new DateTime(2005, 7, 25, 7, 29, 30, 995, DateTimeKind.Local).AddTicks(9010), "Jeramy61@hotmail.com", "Lora", "Hirthe", new DateTime(2019, 6, 4, 5, 0, 37, 163, DateTimeKind.Local).AddTicks(4062), 1 },
                    { 35, new DateTime(2015, 2, 3, 21, 8, 11, 436, DateTimeKind.Local).AddTicks(6931), "Enrico.Volkman@yahoo.com", "Wilton", "Denesik", new DateTime(2019, 7, 7, 1, 27, 56, 610, DateTimeKind.Local).AddTicks(226), 1 },
                    { 40, new DateTime(2010, 1, 31, 19, 28, 56, 895, DateTimeKind.Local).AddTicks(876), "Abel97@hotmail.com", "Dion", "Brekke", new DateTime(2019, 7, 14, 6, 8, 52, 424, DateTimeKind.Local).AddTicks(432), 1 },
                    { 42, new DateTime(1993, 6, 30, 8, 43, 49, 935, DateTimeKind.Local).AddTicks(1143), "Carey.Fritsch@gmail.com", "Crystal", "Gutmann", new DateTime(2019, 5, 5, 6, 47, 45, 839, DateTimeKind.Local).AddTicks(5526), 1 },
                    { 43, new DateTime(2016, 12, 26, 20, 8, 59, 226, DateTimeKind.Local).AddTicks(1111), "Wilmer_Carter@yahoo.com", "Dewayne", "Renner", new DateTime(2019, 6, 26, 8, 5, 42, 394, DateTimeKind.Local).AddTicks(6112), 1 },
                    { 47, new DateTime(2005, 11, 10, 13, 47, 3, 26, DateTimeKind.Local).AddTicks(9578), "Marcelo.Nicolas@hotmail.com", "Harold", "DuBuque", new DateTime(2019, 6, 28, 22, 10, 14, 422, DateTimeKind.Local).AddTicks(145), 1 },
                    { 39, new DateTime(2017, 12, 14, 12, 53, 7, 188, DateTimeKind.Local).AddTicks(1028), "Braxton33@hotmail.com", "Ashtyn", "Quitzon", new DateTime(2019, 5, 10, 23, 45, 50, 137, DateTimeKind.Local).AddTicks(8811), 5 },
                    { 3, new DateTime(2017, 7, 30, 16, 17, 30, 961, DateTimeKind.Local).AddTicks(1164), "Nicole45@hotmail.com", "Curt", "Marvin", new DateTime(2019, 6, 1, 20, 31, 20, 976, DateTimeKind.Local).AddTicks(6055), 2 },
                    { 11, new DateTime(1993, 8, 27, 22, 17, 17, 307, DateTimeKind.Local).AddTicks(5048), "Itzel_Kiehn@hotmail.com", "Aliza", "Rutherford", new DateTime(2019, 7, 15, 12, 20, 41, 404, DateTimeKind.Local).AddTicks(425), 2 },
                    { 15, new DateTime(2009, 8, 3, 22, 47, 45, 703, DateTimeKind.Local).AddTicks(1192), "Zoe_Padberg@yahoo.com", "Cristopher", "Ryan", new DateTime(2019, 6, 18, 15, 6, 30, 506, DateTimeKind.Local).AddTicks(6895), 2 },
                    { 16, new DateTime(1999, 4, 20, 18, 11, 16, 869, DateTimeKind.Local).AddTicks(3119), "Linnie33@hotmail.com", "Julian", "Grady", new DateTime(2019, 6, 28, 14, 55, 17, 649, DateTimeKind.Local).AddTicks(8477), 2 },
                    { 18, new DateTime(1997, 10, 7, 22, 19, 35, 869, DateTimeKind.Local).AddTicks(9056), "Monserrat_Ferry76@gmail.com", "Emil", "Brown", new DateTime(2019, 6, 29, 21, 17, 3, 718, DateTimeKind.Local).AddTicks(1791), 2 },
                    { 29, new DateTime(1989, 11, 25, 22, 19, 55, 792, DateTimeKind.Local).AddTicks(7397), "Hadley.Monahan@yahoo.com", "Fannie", "Dickinson", new DateTime(2019, 6, 23, 6, 54, 39, 797, DateTimeKind.Local).AddTicks(2367), 2 },
                    { 34, new DateTime(1991, 12, 21, 9, 13, 12, 100, DateTimeKind.Local).AddTicks(276), "Jamil_Mills@gmail.com", "Noemie", "Hand", new DateTime(2019, 6, 13, 6, 36, 20, 713, DateTimeKind.Local).AddTicks(6274), 2 },
                    { 50, new DateTime(2012, 9, 26, 14, 24, 32, 864, DateTimeKind.Local).AddTicks(5384), "Eden_Stamm@hotmail.com", "Federico", "Waelchi", new DateTime(2019, 7, 8, 12, 20, 9, 794, DateTimeKind.Local).AddTicks(6964), 2 },
                    { 7, new DateTime(2011, 3, 20, 17, 9, 39, 202, DateTimeKind.Local).AddTicks(107), "Gretchen_Bradtke@yahoo.com", "Abdiel", "Rempel", new DateTime(2019, 6, 25, 13, 39, 28, 40, DateTimeKind.Local).AddTicks(9592), 3 },
                    { 13, new DateTime(2005, 7, 10, 23, 12, 48, 826, DateTimeKind.Local).AddTicks(9681), "Molly_Bergnaum@hotmail.com", "Sigrid", "Pouros", new DateTime(2019, 6, 22, 2, 24, 39, 579, DateTimeKind.Local).AddTicks(7483), 3 },
                    { 17, new DateTime(1987, 4, 29, 21, 24, 22, 636, DateTimeKind.Local).AddTicks(3476), "Lesly_Bartell5@yahoo.com", "Helmer", "Gutkowski", new DateTime(2019, 4, 18, 18, 25, 6, 862, DateTimeKind.Local).AddTicks(2559), 3 },
                    { 10, new DateTime(1976, 12, 28, 7, 9, 21, 117, DateTimeKind.Local).AddTicks(2798), "Wilma.Crooks56@yahoo.com", "Kendrick", "Hansen", new DateTime(2019, 6, 27, 10, 43, 7, 583, DateTimeKind.Local).AddTicks(9862), 2 },
                    { 45, new DateTime(1978, 2, 15, 17, 17, 50, 6, DateTimeKind.Local).AddTicks(6120), "Jessy_Schulist47@gmail.com", "Titus", "Mante", new DateTime(2019, 6, 26, 8, 33, 21, 474, DateTimeKind.Local).AddTicks(5697), 5 }
                });

            migrationBuilder.InsertData(
                table: "Projects",
                columns: new[] { "Id", "AuthorId", "CreationDate", "Deadline", "Description", "Name", "TeamId" },
                values: new object[,]
                {
                    { 1, 2, new DateTime(2017, 2, 19, 21, 53, 20, 150, DateTimeKind.Local).AddTicks(8055), new DateTime(2020, 9, 19, 12, 36, 3, 3, DateTimeKind.Local).AddTicks(1465), @"Laborum nihil atque itaque ea sed quisquam dicta et eum.
                Quisquam maxime consequatur quaerat autem in.", "autem", 1 },
                    { 2, 3, new DateTime(2016, 8, 17, 19, 34, 47, 242, DateTimeKind.Local).AddTicks(4345), new DateTime(2021, 3, 23, 9, 59, 33, 146, DateTimeKind.Local).AddTicks(9089), @"Veritatis ad culpa odit ea porro expedita.
                Nostrum fuga at sed deserunt in ipsam.
                Ut pariatur atque laudantium non non est.", "est", 2 },
                    { 3, 7, new DateTime(2019, 2, 22, 8, 53, 46, 371, DateTimeKind.Local).AddTicks(846), new DateTime(2021, 7, 14, 14, 26, 27, 380, DateTimeKind.Local).AddTicks(6291), @"Ut sapiente neque eos debitis.
                Dolorum laborum soluta vitae.
                Aut et sed voluptatem quisquam repellendus repellat dicta.
                Ullam perspiciatis neque et.", "tempore", 3 },
                    { 4, 4, new DateTime(2019, 1, 5, 15, 31, 33, 384, DateTimeKind.Local).AddTicks(9654), new DateTime(2019, 8, 23, 6, 6, 47, 935, DateTimeKind.Local).AddTicks(5952), @"Voluptatibus ipsa recusandae dicta.
                Autem minus cupiditate amet tenetur molestiae dolorem ea id voluptate.
                Quos voluptas vero maiores error qui.
                Inventore asperiores quibusdam ea ducimus.
                Praesentium a sunt ab molestiae quod voluptas et.", "cupiditate", 4 },
                    { 5, 1, new DateTime(2019, 5, 18, 10, 44, 15, 465, DateTimeKind.Local).AddTicks(2297), new DateTime(2020, 5, 9, 5, 28, 23, 715, DateTimeKind.Local).AddTicks(804), @"Autem vel omnis veritatis consequatur.
                Dolor ut nam facilis deserunt in est officiis.
                Sunt sit veniam.
                Qui repellat fugiat fugit deleniti repellat rerum alias praesentium.
                Excepturi recusandae dicta nobis architecto soluta laborum.
                Exercitationem dignissimos fugit.", "voluptatum", 5 }
                });

            migrationBuilder.InsertData(
                table: "ProjectTasks",
                columns: new[] { "Id", "DateOfCreation", "Description", "FinishedAt", "Name", "PerformerId", "PojectId", "TaskStateId" },
                values: new object[,]
                {
                    { 9, new DateTime(2014, 11, 24, 22, 8, 30, 652, DateTimeKind.Local).AddTicks(9839), @"Ea dolor aut incidunt asperiores laborum id.
                Ex quia vero ut amet quibusdam saepe.
                Laboriosam incidunt cumque.
                Labore inventore sint deserunt veniam saepe.
                Possimus et nulla qui officia quia et provident pariatur.", new DateTime(2019, 10, 1, 5, 50, 32, 118, DateTimeKind.Local).AddTicks(7290), "quis", 14, 1, 3 },
                    { 14, new DateTime(2018, 5, 27, 13, 0, 57, 523, DateTimeKind.Local).AddTicks(5285), @"Inventore maiores at molestias quas laborum labore dolorem velit.
                Debitis blanditiis maxime et aperiam asperiores earum.
                Nostrum magni quaerat quasi.
                Minus sed rem et non.", new DateTime(2021, 5, 20, 12, 19, 31, 528, DateTimeKind.Local).AddTicks(6041), "velit", 19, 4, 2 },
                    { 21, new DateTime(2015, 4, 22, 5, 4, 32, 99, DateTimeKind.Local).AddTicks(9039), @"Maxime velit distinctio voluptatem.
                Sit aut provident voluptas dicta praesentium ea.
                Natus eos magni occaecati quis nisi delectus ducimus.", new DateTime(2020, 1, 27, 14, 54, 6, 129, DateTimeKind.Local).AddTicks(8176), "repellat", 26, 4, 4 },
                    { 26, new DateTime(2019, 2, 10, 20, 23, 8, 361, DateTimeKind.Local).AddTicks(9644), @"Dolorem animi aperiam quia pariatur qui recusandae cupiditate mollitia error.
                Repudiandae nihil dignissimos eos placeat impedit nesciunt deleniti.
                Veritatis ut pariatur soluta.
                Delectus optio voluptatem omnis quia iusto incidunt saepe ipsam eligendi.
                Beatae nemo nulla sit ea.", new DateTime(2020, 2, 21, 1, 55, 26, 593, DateTimeKind.Local).AddTicks(8412), "est", 31, 4, 3 },
                    { 28, new DateTime(2018, 6, 22, 14, 49, 34, 422, DateTimeKind.Local).AddTicks(8639), @"Et culpa neque ut dolor fugiat praesentium officiis in quidem.
                Voluptatem aut asperiores illum totam esse ut.
                Vel aut non laborum repudiandae nisi perferendis.", new DateTime(2020, 1, 28, 20, 16, 57, 947, DateTimeKind.Local).AddTicks(7490), "hic", 33, 4, 1 },
                    { 33, new DateTime(2017, 2, 26, 17, 25, 11, 493, DateTimeKind.Local).AddTicks(4000), @"Quasi asperiores dolor voluptatem distinctio similique quod voluptatem sequi odio.
                Vel tenetur possimus delectus deserunt sint.
                Sed tenetur maxime omnis dolorem qui cumque nihil.", new DateTime(2021, 5, 19, 13, 59, 44, 520, DateTimeKind.Local).AddTicks(246), "tempore", 38, 4, 1 },
                    { 36, new DateTime(2016, 1, 20, 17, 42, 37, 381, DateTimeKind.Local).AddTicks(3166), @"Quasi placeat minima.
                Quas vitae earum dolores delectus quia sit deleniti modi aspernatur.
                Quia dignissimos eligendi voluptatum reprehenderit numquam.
                Excepturi distinctio et.
                Molestiae aut voluptatem harum voluptatibus ut tempora enim reiciendis.", new DateTime(2019, 7, 23, 15, 30, 3, 176, DateTimeKind.Local).AddTicks(5811), "ad", 41, 4, 2 },
                    { 39, new DateTime(2018, 1, 14, 2, 28, 26, 840, DateTimeKind.Local).AddTicks(4523), @"Maiores nihil minima reiciendis rem consequuntur repudiandae quis.
                Velit nesciunt consequatur eum commodi laborum.
                Sapiente aliquam praesentium et ipsam culpa libero adipisci velit.", new DateTime(2022, 4, 22, 20, 36, 18, 476, DateTimeKind.Local).AddTicks(1882), "nam", 44, 4, 2 },
                    { 41, new DateTime(2017, 4, 21, 4, 50, 45, 908, DateTimeKind.Local).AddTicks(6773), @"Harum et commodi perferendis expedita blanditiis.
                Esse cum perferendis nulla et quo sequi laudantium.
                Excepturi expedita non est.", new DateTime(2020, 5, 9, 1, 18, 37, 186, DateTimeKind.Local).AddTicks(2433), "optio", 46, 4, 3 },
                    { 43, new DateTime(2019, 7, 3, 0, 41, 20, 714, DateTimeKind.Local).AddTicks(1008), @"Sunt dolore soluta sapiente dolorem est nesciunt in sunt veritatis.
                Nemo incidunt quia laboriosam et possimus distinctio facere eum.
                Praesentium fugiat quisquam dolorem ratione sapiente necessitatibus sed eius.
                Est est expedita hic impedit nemo temporibus consequuntur error.
                Fugit odit aut velit quae.
                Porro dolorem molestiae voluptatem veritatis ipsum mollitia aut aut velit.", new DateTime(2020, 5, 6, 22, 13, 8, 190, DateTimeKind.Local).AddTicks(8170), "a", 48, 4, 4 },
                    { 1, new DateTime(2018, 2, 13, 7, 46, 21, 7, DateTimeKind.Local).AddTicks(2007), @"Fuga recusandae ipsum.
                Assumenda cumque ipsum minima ut.
                Minima et commodi harum porro.", new DateTime(2020, 12, 17, 13, 37, 55, 327, DateTimeKind.Local).AddTicks(93), "in", 5, 5, 2 },
                    { 2, new DateTime(2019, 3, 16, 18, 56, 47, 900, DateTimeKind.Local).AddTicks(4058), @"Ab quisquam voluptas distinctio dignissimos earum.
                Nihil voluptatem et error autem cupiditate.", new DateTime(2020, 7, 26, 7, 59, 52, 498, DateTimeKind.Local).AddTicks(991), "sit", 6, 5, 3 },
                    { 3, new DateTime(2019, 4, 29, 8, 56, 3, 651, DateTimeKind.Local).AddTicks(4406), @"Omnis incidunt beatae atque error.
                Enim optio dolorem harum praesentium et blanditiis quia.", new DateTime(2020, 7, 16, 21, 10, 5, 271, DateTimeKind.Local).AddTicks(6010), "id", 8, 5, 4 },
                    { 4, new DateTime(2019, 4, 29, 18, 50, 57, 969, DateTimeKind.Local).AddTicks(2548), @"Velit quibusdam aliquam consectetur.
                Delectus perspiciatis quia.
                Illum ab dolores nostrum.
                Omnis quia aut nostrum qui consequatur.", new DateTime(2019, 10, 1, 11, 46, 13, 476, DateTimeKind.Local).AddTicks(111), "voluptas", 9, 5, 1 },
                    { 7, new DateTime(2018, 12, 2, 2, 34, 19, 75, DateTimeKind.Local).AddTicks(1706), @"Nihil officia et magnam porro sint pariatur repudiandae accusantium.
                Laboriosam ut quia iusto est.
                Laboriosam sit voluptatem cumque quia et incidunt maxime quis atque.", new DateTime(2021, 5, 28, 7, 35, 10, 67, DateTimeKind.Local).AddTicks(9970), "et", 12, 5, 1 },
                    { 19, new DateTime(2019, 4, 24, 16, 11, 29, 165, DateTimeKind.Local).AddTicks(5219), @"Quo hic nihil sed ex nam.
                Ullam saepe saepe est vel quo voluptates amet nostrum qui.
                Alias totam aperiam est quia quas.", new DateTime(2020, 2, 7, 21, 11, 57, 245, DateTimeKind.Local).AddTicks(6985), "perspiciatis", 24, 5, 3 },
                    { 23, new DateTime(2016, 3, 31, 10, 15, 15, 226, DateTimeKind.Local).AddTicks(7462), @"Dolore rem rem id soluta sit sit.
                Exercitationem ea perferendis omnis et praesentium laborum odit dicta velit.
                Provident rerum non ea cupiditate.
                Dolores eligendi maiores mollitia temporibus.
                Illo sit assumenda ut omnis temporibus.", new DateTime(2019, 11, 10, 19, 45, 48, 338, DateTimeKind.Local).AddTicks(6471), "culpa", 28, 5, 4 },
                    { 27, new DateTime(2017, 8, 13, 0, 29, 18, 904, DateTimeKind.Local).AddTicks(5911), @"Molestias eos consectetur ipsa atque.
                Veniam facere ex non aut quia dolor voluptas enim.
                Qui sit iure facere.
                Tempore in ipsam laudantium pariatur quis delectus.
                Commodi qui velit impedit vel aliquam praesentium dolore.
                Voluptate voluptatum voluptate unde.", new DateTime(2021, 4, 12, 5, 38, 9, 724, DateTimeKind.Local).AddTicks(5477), "quisquam", 32, 5, 3 },
                    { 31, new DateTime(2019, 3, 16, 16, 7, 12, 394, DateTimeKind.Local).AddTicks(3271), @"Officia error maiores iure quas quasi dolorum porro id.
                Voluptatibus officiis quo velit eligendi optio dolorem sapiente optio ex.", new DateTime(2020, 5, 18, 19, 19, 24, 569, DateTimeKind.Local).AddTicks(4411), "aut", 36, 5, 1 },
                    { 32, new DateTime(2017, 10, 2, 4, 47, 26, 891, DateTimeKind.Local).AddTicks(1690), @"Eligendi molestiae qui.
                Laborum autem quasi.
                Est aspernatur ut sint sed.
                Atque nihil est nobis quaerat ducimus.
                Id ipsum at dolores eligendi.
                Provident qui voluptatibus perspiciatis et quia et voluptatem.", new DateTime(2019, 11, 17, 0, 19, 34, 44, DateTimeKind.Local).AddTicks(9293), "vel", 37, 5, 3 },
                    { 44, new DateTime(2018, 6, 13, 7, 10, 1, 23, DateTimeKind.Local).AddTicks(1852), @"Sunt qui in quaerat et.
                Delectus sed consectetur illo cumque ex qui.
                Dolor sit et veritatis minima et quo.", new DateTime(2020, 6, 3, 5, 59, 12, 199, DateTimeKind.Local).AddTicks(4393), "animi", 49, 3, 2 },
                    { 34, new DateTime(2018, 7, 20, 2, 40, 48, 988, DateTimeKind.Local).AddTicks(5252), @"Aspernatur eos ipsam perspiciatis voluptatibus delectus et nam reprehenderit.
                Fuga est et illo sequi.
                Laboriosam laborum mollitia reprehenderit tempora.
                Dolorem repellendus ducimus labore eveniet odio cupiditate.
                Occaecati sit vero consectetur distinctio odit minima autem.
                Sunt ullam vitae nesciunt consequatur assumenda perferendis ea.", new DateTime(2019, 11, 6, 11, 25, 56, 22, DateTimeKind.Local).AddTicks(2324), "et", 39, 5, 2 },
                    { 25, new DateTime(2019, 1, 9, 8, 25, 5, 577, DateTimeKind.Local).AddTicks(7566), @"Sapiente omnis eos tempora pariatur voluptatem quas ea impedit sint.
                Molestias modi magni ex sit saepe repellendus.
                Officia eius sint ut aut sint eligendi quo exercitationem.
                Dolorem libero earum sint quaerat.", new DateTime(2020, 3, 27, 1, 21, 14, 249, DateTimeKind.Local).AddTicks(9857), "et", 30, 3, 2 },
                    { 18, new DateTime(2018, 9, 20, 16, 46, 8, 276, DateTimeKind.Local).AddTicks(3942), @"Quo dicta in eos expedita.
                Exercitationem fugit commodi explicabo.
                Quia architecto vel.", new DateTime(2019, 9, 26, 0, 37, 32, 89, DateTimeKind.Local).AddTicks(388), "possimus", 23, 3, 1 },
                    { 15, new DateTime(2019, 3, 15, 22, 16, 2, 476, DateTimeKind.Local).AddTicks(421), @"Voluptatem sapiente amet non ea quibusdam quidem enim.
                Temporibus inventore ut cumque reprehenderit in fugit quia nostrum laborum.
                Tempore sed ut aperiam maxime quo facere dignissimos corporis voluptatem.
                In quidem corporis tempore voluptatibus maiores commodi est praesentium.
                Alias quod placeat velit harum dolorem.", new DateTime(2020, 7, 4, 21, 59, 24, 463, DateTimeKind.Local).AddTicks(9146), "voluptatem", 20, 1, 2 },
                    { 16, new DateTime(2015, 9, 20, 14, 5, 18, 903, DateTimeKind.Local).AddTicks(6889), @"Velit doloribus sequi.
                Est quae consectetur aut.
                Rerum distinctio mollitia in porro deserunt magni omnis occaecati sequi.
                Tempore vel eius dolorum fugit rerum qui.
                Consectetur quis occaecati.
                Fugiat dolore voluptatum doloribus eum dolor quia velit.", new DateTime(2021, 10, 17, 3, 41, 40, 353, DateTimeKind.Local).AddTicks(4244), "et", 21, 1, 3 },
                    { 17, new DateTime(2019, 2, 22, 23, 58, 21, 401, DateTimeKind.Local).AddTicks(1033), @"Et voluptatibus aut aut culpa.
                Iure illum assumenda sed et.
                Voluptatem qui illum est molestiae repellat voluptas sequi et.
                Ullam culpa qui quod nemo soluta et.", new DateTime(2020, 5, 21, 2, 7, 36, 181, DateTimeKind.Local).AddTicks(4559), "esse", 22, 1, 1 },
                    { 22, new DateTime(2014, 10, 15, 4, 51, 5, 593, DateTimeKind.Local).AddTicks(1049), @"Labore voluptas doloribus non inventore aut tenetur quibusdam perspiciatis beatae.
                Tenetur libero consequatur deleniti ut voluptatem sit occaecati voluptas ut.
                Enim qui voluptate quod maxime.", new DateTime(2019, 12, 31, 17, 39, 16, 952, DateTimeKind.Local).AddTicks(5003), "quaerat", 27, 1, 3 },
                    { 30, new DateTime(2017, 5, 12, 7, 10, 47, 500, DateTimeKind.Local).AddTicks(3872), @"Nulla repellat culpa ut sed voluptas vero id.
                Et odio veniam quia enim ad.
                Tempore in tempore deserunt aut consequuntur voluptatem.
                Veniam distinctio magni expedita voluptates impedit atque voluptatum.
                Qui neque eius consequatur libero natus aut.", new DateTime(2021, 3, 4, 9, 34, 46, 294, DateTimeKind.Local).AddTicks(8951), "est", 35, 1, 1 },
                    { 35, new DateTime(2018, 4, 2, 23, 49, 34, 633, DateTimeKind.Local).AddTicks(3363), @"In aperiam non explicabo maxime et numquam.
                Nam voluptas dignissimos eum.
                Qui quae voluptatem delectus accusamus accusantium explicabo ratione commodi sit.
                Iusto aut minima.", new DateTime(2020, 1, 1, 8, 59, 31, 468, DateTimeKind.Local).AddTicks(156), "nostrum", 40, 1, 3 },
                    { 37, new DateTime(2016, 8, 5, 14, 50, 7, 422, DateTimeKind.Local).AddTicks(6507), @"Optio magnam in.
                Consectetur iste occaecati.
                Assumenda vel voluptatem accusamus illo ut et magni porro.
                Eum voluptatum suscipit laboriosam.
                Magnam voluptates quia velit iusto sunt ipsum laudantium.
                Ut ut vel velit.", new DateTime(2020, 7, 14, 14, 7, 24, 802, DateTimeKind.Local).AddTicks(4738), "laudantium", 42, 1, 1 },
                    { 38, new DateTime(2019, 4, 21, 11, 41, 43, 189, DateTimeKind.Local).AddTicks(6507), @"Delectus dignissimos molestiae eaque et quod consequatur at.
                Iure quo ex est voluptatem voluptatem.
                Ea dicta velit.", new DateTime(2019, 9, 15, 5, 45, 55, 104, DateTimeKind.Local).AddTicks(2688), "animi", 43, 1, 4 },
                    { 42, new DateTime(2017, 11, 17, 10, 35, 40, 569, DateTimeKind.Local).AddTicks(3602), @"Molestias tenetur sunt accusamus molestias eos et dolor dolor nisi.
                Ab doloremque quia et optio cumque culpa occaecati a doloribus.
                Nostrum suscipit corrupti eligendi soluta quasi inventore aliquid velit.", new DateTime(2021, 9, 18, 0, 31, 54, 398, DateTimeKind.Local).AddTicks(3062), "molestiae", 47, 1, 3 },
                    { 5, new DateTime(2018, 8, 15, 8, 22, 46, 180, DateTimeKind.Local).AddTicks(5142), @"Laboriosam temporibus laudantium aut.
                Harum illum minima incidunt autem culpa corporis sequi cumque.
                Facilis vel voluptatem eum fugit dolorem.
                Nulla ipsa doloribus omnis et reprehenderit eligendi nemo quia.", new DateTime(2020, 10, 5, 18, 36, 37, 640, DateTimeKind.Local).AddTicks(3026), "eos", 10, 2, 1 },
                    { 6, new DateTime(2018, 8, 3, 23, 14, 38, 430, DateTimeKind.Local).AddTicks(8003), @"Expedita cum deleniti rerum quas.
                Quo pariatur et officia laudantium error placeat autem.", new DateTime(2020, 10, 17, 22, 29, 37, 564, DateTimeKind.Local).AddTicks(8226), "laudantium", 11, 2, 2 },
                    { 10, new DateTime(2017, 5, 13, 11, 10, 18, 437, DateTimeKind.Local).AddTicks(2931), @"Et vero voluptas qui expedita sint placeat facilis eum consectetur.
                Ullam distinctio nostrum hic aspernatur possimus.
                Optio excepturi dolor ut ipsum natus voluptatem quae porro.
                Corporis ipsam ea ut et et libero.
                Illo enim aspernatur ut aspernatur et.
                Voluptatibus et ut amet vitae vel eum et aliquam.", new DateTime(2020, 11, 27, 10, 44, 11, 584, DateTimeKind.Local).AddTicks(4366), "nemo", 15, 2, 1 },
                    { 11, new DateTime(2017, 10, 26, 12, 53, 49, 635, DateTimeKind.Local).AddTicks(6931), @"Et ipsum nisi sint fugiat voluptatem illo.
                Dolorum cupiditate quae.
                Facere ut velit cum excepturi consectetur.", new DateTime(2021, 1, 22, 3, 7, 16, 391, DateTimeKind.Local).AddTicks(3638), "ipsam", 16, 2, 2 },
                    { 13, new DateTime(2018, 1, 1, 11, 27, 10, 404, DateTimeKind.Local).AddTicks(5299), @"Eum qui modi sed sequi id dolor ad provident.
                Quibusdam atque sint ut eaque.
                At porro quia totam.
                Ut animi delectus quae voluptas accusamus dolore aut.", new DateTime(2020, 2, 13, 14, 33, 6, 93, DateTimeKind.Local).AddTicks(8966), "nam", 18, 2, 1 },
                    { 24, new DateTime(2017, 11, 27, 11, 28, 42, 837, DateTimeKind.Local).AddTicks(2576), @"Sapiente sed et velit voluptas sed.
                Sed sed expedita porro quasi.", new DateTime(2020, 3, 26, 3, 25, 33, 354, DateTimeKind.Local).AddTicks(2926), "hic", 29, 2, 1 },
                    { 29, new DateTime(2019, 1, 24, 6, 55, 46, 563, DateTimeKind.Local).AddTicks(6109), @"Odio vel magnam rem doloremque quam repellat autem est.
                Et sunt sed qui minima quos voluptatum laborum dolores.
                Aspernatur quo ut magnam saepe voluptas.
                Sequi sint consequatur consequatur enim et magnam omnis consequatur eius.
                Itaque explicabo vel quas animi voluptas doloribus aliquam deleniti modi.", new DateTime(2021, 2, 10, 14, 35, 11, 24, DateTimeKind.Local).AddTicks(7757), "accusantium", 34, 2, 2 },
                    { 45, new DateTime(2019, 2, 27, 3, 26, 16, 521, DateTimeKind.Local).AddTicks(1893), @"Facere inventore quod.
                Qui aut laudantium iure praesentium adipisci voluptas amet cupiditate.
                Ut velit possimus deserunt iusto ea quo similique quia aspernatur.
                Sunt corrupti qui aperiam esse.
                Dolore ipsam et iusto asperiores.", new DateTime(2020, 3, 4, 17, 53, 57, 734, DateTimeKind.Local).AddTicks(9098), "sapiente", 50, 2, 2 },
                    { 8, new DateTime(2017, 2, 16, 8, 4, 10, 886, DateTimeKind.Local).AddTicks(3727), @"Magnam necessitatibus accusantium aliquam asperiores et alias quo error.
                Tempora omnis sed accusantium.", new DateTime(2020, 5, 30, 7, 31, 40, 141, DateTimeKind.Local).AddTicks(6540), "vel", 13, 3, 1 },
                    { 12, new DateTime(2016, 8, 21, 20, 55, 59, 279, DateTimeKind.Local).AddTicks(1469), @"Molestiae itaque necessitatibus.
                Quod est tenetur.
                Unde molestiae nesciunt et voluptatem explicabo.
                Qui praesentium nostrum dolor.
                Quas sapiente saepe quae.", new DateTime(2020, 4, 15, 21, 9, 50, 638, DateTimeKind.Local).AddTicks(270), "qui", 17, 3, 2 },
                    { 20, new DateTime(2017, 2, 22, 10, 19, 0, 959, DateTimeKind.Local).AddTicks(3211), @"Sint nemo dolore fuga sequi blanditiis et.
                Quia et velit quia error optio libero nulla.", new DateTime(2021, 8, 3, 6, 1, 46, 975, DateTimeKind.Local).AddTicks(761), "vitae", 25, 3, 2 },
                    { 40, new DateTime(2016, 11, 7, 0, 15, 4, 359, DateTimeKind.Local).AddTicks(8635), @"Architecto qui et aspernatur velit labore tempora eligendi eos.
                Accusamus velit porro consequatur labore quod enim natus.
                Sapiente ea ut ea facere vitae error consequatur sint.
                In ut ea ut molestias possimus ut.
                Quo unde perferendis incidunt.", new DateTime(2022, 7, 9, 4, 14, 21, 117, DateTimeKind.Local).AddTicks(28), "aut", 45, 5, 4 }
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 6);

            migrationBuilder.DeleteData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 7);

            migrationBuilder.DeleteData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 8);

            migrationBuilder.DeleteData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 9);

            migrationBuilder.DeleteData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 10);

            migrationBuilder.DeleteData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 11);

            migrationBuilder.DeleteData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 12);

            migrationBuilder.DeleteData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 13);

            migrationBuilder.DeleteData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 14);

            migrationBuilder.DeleteData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 15);

            migrationBuilder.DeleteData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 16);

            migrationBuilder.DeleteData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 17);

            migrationBuilder.DeleteData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 18);

            migrationBuilder.DeleteData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 19);

            migrationBuilder.DeleteData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 20);

            migrationBuilder.DeleteData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 21);

            migrationBuilder.DeleteData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 22);

            migrationBuilder.DeleteData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 23);

            migrationBuilder.DeleteData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 24);

            migrationBuilder.DeleteData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 25);

            migrationBuilder.DeleteData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 26);

            migrationBuilder.DeleteData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 27);

            migrationBuilder.DeleteData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 28);

            migrationBuilder.DeleteData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 29);

            migrationBuilder.DeleteData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 30);

            migrationBuilder.DeleteData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 31);

            migrationBuilder.DeleteData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 32);

            migrationBuilder.DeleteData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 33);

            migrationBuilder.DeleteData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 34);

            migrationBuilder.DeleteData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 35);

            migrationBuilder.DeleteData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 36);

            migrationBuilder.DeleteData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 37);

            migrationBuilder.DeleteData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 38);

            migrationBuilder.DeleteData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 39);

            migrationBuilder.DeleteData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 40);

            migrationBuilder.DeleteData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 41);

            migrationBuilder.DeleteData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 42);

            migrationBuilder.DeleteData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 43);

            migrationBuilder.DeleteData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 44);

            migrationBuilder.DeleteData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 45);

            migrationBuilder.DeleteData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "TaskStates",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "TaskStates",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "TaskStates",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "TaskStates",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 6);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 8);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 9);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 10);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 11);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 12);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 13);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 14);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 15);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 16);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 17);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 18);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 19);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 20);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 21);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 22);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 23);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 24);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 25);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 26);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 27);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 28);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 29);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 30);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 31);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 32);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 33);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 34);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 35);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 36);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 37);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 38);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 39);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 40);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 41);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 42);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 43);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 44);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 45);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 46);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 47);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 48);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 49);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 50);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 7);

            migrationBuilder.DeleteData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 5);
        }
    }
}
