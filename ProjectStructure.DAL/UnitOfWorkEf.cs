using System;
using System.Threading.Tasks;
using ProjectStructure.Common;
using ProjectStructure.DAL.Context;
using ProjectStructure.DAL.Entities;
using ProjectStructure.DAL.Interfaces;
using ProjectStructure.DAL.Repositories;

namespace ProjectStructure.DAL
{
    public class UnitOfWorkEf : IUnitOfWorkAsync
    {
        
        private readonly ProjectDbContext _context;

        private IRepositoryAsync<Project> _projectRepository;

        private IRepositoryAsync<User> _userRepository;

        private IRepositoryAsync<Team> _teamRepository;

        private IRepositoryAsync<TaskState> _taskStateRepository;

        private IRepositoryAsync<ProjectTask> _projectTaskRepository;

        public UnitOfWorkEf(ProjectDbContext context)
        {
            _context = context;
        }

        public IRepositoryAsync<Project> ProjectRepository
        {
            get
            {
                if (_projectRepository == null)
                {
                    _projectRepository = new GenericRepositoryAsync<Project>(_context);
                }
                return _projectRepository;
            }
        }

        public IRepositoryAsync<User> UserRepository
        {
            get
            {
                if (_userRepository == null)
                {
                    _userRepository = new GenericRepositoryAsync<User>(_context);
                }
                return _userRepository;
            }
        }
  
        public IRepositoryAsync<Team> TeamRepository 
        {
            get
            {
                if (_teamRepository == null)
                {
                    _teamRepository = new GenericRepositoryAsync<Team>(_context);
                }
                return _teamRepository;
            }
        }
        

        public IRepositoryAsync<TaskState> TaskStateRepository 
        {
            get
            {
                if (_taskStateRepository == null)
                {
                    _taskStateRepository = new GenericRepositoryAsync<TaskState>(_context);
                }
                return _taskStateRepository;
            }
        }

      

        public IRepositoryAsync<ProjectTask> ProjectTaskRepository 
        {
            get
            {
                if (_projectTaskRepository == null)
                {
                    _projectTaskRepository = new GenericRepositoryAsync<ProjectTask>(_context);
                }
                return _projectTaskRepository;
            }
        }
        
    

        public void Dispose()
        {
            _context?.Dispose();
        }

        public async Task<int> SaveChangesAsync()
        {
           return await _context.SaveChangesAsync();
        }
    }
}