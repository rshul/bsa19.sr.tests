﻿using Microsoft.EntityFrameworkCore;
using ProjectStructure.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace ProjectStructure.DAL.Context
{
    public class ProjectDbContext : DbContext
    {
        public ProjectDbContext(DbContextOptions<ProjectDbContext> options) : base(options)
        {
        }

        public DbSet<User> Users { get; set; }
        public DbSet<Project> Projects { get; set; }
        public DbSet<ProjectTask> ProjectTasks { get; set; }
        public DbSet<Team> Teams { get; set; }
        public DbSet<TaskState> TaskStates { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Project>(project =>
            {
                project.HasOne(p => p.Author)
                    .WithOne(a => a.Project)
                    .HasForeignKey<Project>(p => p.AuthorId)
                    .OnDelete(DeleteBehavior.SetNull);

                project.HasMany(p => p.Tasks)
                    .WithOne(t => t.Poject);

                project.Property(p => p.CreatedAt).HasColumnName("CreationDate");


            });

            modelBuilder.Entity<ProjectTask>(task =>
            {
                task.HasOne(t => t.Performer)
                    .WithOne(p => p.Task)
                    .HasForeignKey<ProjectTask>(t => t.PerformerId)
                    .OnDelete(DeleteBehavior.SetNull);

                task.HasOne(p => p.Poject)
                    .WithMany(p => p.Tasks)
                    .HasForeignKey(t => t.PojectId)
                    .OnDelete(DeleteBehavior.Cascade);

                task.Property(t => t.Name).IsRequired().HasMaxLength(80);
                task.Property(t => t.CreatedAt).HasColumnName("DateOfCreation");
            });

            modelBuilder.Entity<User>(user =>
            {
                user.HasOne(u => u.Team)
                    .WithMany(t => t.Members)
                    .HasForeignKey(u => u.TeamId)
                    .OnDelete(DeleteBehavior.SetNull);

                user.Property(u => u.FirstName).IsRequired().HasMaxLength(80);
                user.Property(u => u.LastName).IsRequired().HasMaxLength(80);
                user.Property(u => u.TeamId).IsRequired();
                user.Property(u => u.RegisteredAt).HasColumnName("RegistrationDate");
            });
            
            modelBuilder.Entity<Team>(entity =>
            {
                entity.HasKey(e => e.Id);
                entity.Property(e => e.Name).HasColumnName("TeamName").HasMaxLength(80);
                entity.Property(e => e.CreatedAt).HasColumnName("CreationDate");

                entity.HasMany(t => t.Members)
                    .WithOne(m => m.Team);
            });

           

            //modelBuilder.Seed();

        }


    }
}
