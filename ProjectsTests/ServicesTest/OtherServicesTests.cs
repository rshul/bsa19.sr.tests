﻿using AutoMapper;
using FakeItEasy;
using NUnit.Framework;
using ProjectStructure.BLL.Interfaces;
using ProjectStructure.BLL.Services;
using ProjectStructure.Common;
using ProjectStructure.DAL.Entities;
using ProjectStructure.DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ProjectsTests.ServicesTest
{
    [TestFixture]
    public class OtherServicesTests
    {
        private IUnitOfWorkAsync _fakeUow;
        private IRepositoryAsync<Project> _fakeProjectsRepo;
        private IRepositoryAsync<ProjectTask> _fakeTasksRepo;
        private IRepositoryAsync<Team> _fakeTeamsRepo;
        private IRepositoryAsync<User> _fakeUserRepo;
        private IMapper _mapper;
        private IQueueService _fakeQService;
        private IServiceAsyncWithMapping<Project, ProjectDTO, CreatProjectDTO> _projectService;
        private IServiceAsyncWithMapping<User, UserDTO, CreateUserDTO> _userService;
        private IServiceAsyncWithMapping<Team, TeamDTO, CreateTeamDTO> _teamService;
        private IServiceAsyncWithMapping<ProjectTask, ProjectTaskDTO, CreateProjectTaskDTO> _taskService;

        [SetUp]
        public void Setup()
        {
            _fakeUow = A.Fake<IUnitOfWorkAsync>();
            _mapper = A.Fake<IMapper>();
            _fakeQService = A.Fake<IQueueService>();
            _fakeProjectsRepo = A.Fake<IRepositoryAsync<Project>>();
            _fakeTasksRepo = A.Fake<IRepositoryAsync<ProjectTask>>();
            _fakeTeamsRepo = A.Fake<IRepositoryAsync<Team>>();
            _fakeUserRepo = A.Fake<IRepositoryAsync<User>>();
            _projectService = new ProjectServiceAsyncWithMapping(_fakeUow, _mapper, _fakeQService);
            _taskService = new TaskServiceAsyncWithMapping(_fakeUow, _mapper, _fakeQService);
            _userService = new UserServiceAsyncWithMapping(_fakeUow, _mapper, _fakeQService);
            _teamService = new TeamServiceAsyncWithMapping(_fakeUow, _mapper, _fakeQService);
        }

        [TearDown]
        public void TestTearDown()
        {

        }

        [Test]
        public async Task When_Create_User_Then_Call_RepositoryInsertAsync()
        {
            var fCreateUserDto = A.Fake<CreateUserDTO>();
            var fUser = A.Fake<User>();

            A.CallTo(() => _fakeUow.UserRepository).Returns(_fakeUserRepo);
            A.CallTo(() => _fakeUserRepo.InsertAsync(A<User>.Ignored)).Returns(Task.FromResult(fUser));

            var savedProject = await _userService.CreateAsyncMapped(fCreateUserDto);

            A.CallTo(() => _fakeUserRepo.InsertAsync(A<User>.Ignored)).MustHaveHappened();
        }


        [Test]
        public async Task When_UpdateTask_Then_Call_RepositoryUpdateAsync()
        {
           
            var fTask = A.Fake<ProjectTask>();
            var ftaskDto = A.Fake<ProjectTaskDTO>();

            A.CallTo(() => _fakeUow.ProjectTaskRepository).Returns(_fakeTasksRepo);

            await _taskService.UpdateAsyncMapped(ftaskDto);

            A.CallTo(() => _fakeTasksRepo.UpdateAsync(A<int>.Ignored, A<ProjectTask>.Ignored)).MustHaveHappened();
        }

        [Test]
        public async Task When_UpdateUser_The_Call_RepositoryUpdateAsync()
        {

            var fUser = A.Fake<User>();
            var fUserDto = A.Fake<UserDTO>();

            A.CallTo(() => _fakeUow.UserRepository).Returns(_fakeUserRepo);

            await _userService.UpdateAsyncMapped(fUserDto);

            A.CallTo(() => _fakeUserRepo.UpdateAsync(A<int>.Ignored, A<User>.Ignored)).MustHaveHappened();
        }



    }

}
