﻿using AutoMapper;
using FakeItEasy;
using NUnit.Framework;
using ProjectsTests.Fake;
using ProjectStructure.BLL.Interfaces;
using ProjectStructure.BLL.MappingProfiles;
using ProjectStructure.BLL.Services;
using ProjectStructure.Common;
using ProjectStructure.DAL.Entities;
using ProjectStructure.DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace TeProjectsTests.ServicesTeststs
{
    [TestFixture]
    public class ProjectServiceTest
    {
        private readonly IUnitOfWorkAsync _fakeUow;
        private readonly IRepositoryAsync<Project> _fakeRepo;
        private readonly IMapper _mapper;
        private readonly IQueueService _fakeQService;
        private readonly IServiceAsyncWithMapping<Project, ProjectDTO, CreatProjectDTO> _projectService;

        public ProjectServiceTest()
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile<ProjectProfile>();
            });
            var mapper = config.CreateMapper();
            _mapper = mapper;

            _fakeUow = new FakeUnitOfWork(new FakeContext());
            _fakeRepo = _fakeUow.ProjectRepository;
            _fakeQService = new FakeQueService();
             _projectService = new ProjectServiceAsyncWithMapping(_fakeUow, mapper, new FakeQueService() );

        }
        [SetUp]
        public void Setup()
        {
            var createDto = new CreatProjectDTO()
            {
                Author_Id = 2,
                Created_At = new DateTime(2017, 2, 19, 21, 53, 20, 150, DateTimeKind.Local).AddTicks(8055),
                Deadline = new DateTime(2020, 9, 19, 12, 36, 3, 3, DateTimeKind.Local).AddTicks(1465),
                Description = @"Laborum nihil atque itaque ea sed quisquam dicta et eum.
Quisquam maxime consequatur quaerat autem in.",
                Name = "autem",
                Team_Id = 1
            };

            var project = new Project()
            {
                Id = 0,
                AuthorId = 3,
                CreatedAt = new DateTime(2016, 8, 17, 19, 34, 47, 242, DateTimeKind.Local).AddTicks(4345),
                Deadline = new DateTime(2021, 3, 23, 9, 59, 33, 146, DateTimeKind.Local).AddTicks(9089),
                Description = @"Veritatis ad culpa odit ea porro expedita.
Nostrum fuga at sed deserunt in ipsam.
Ut pariatur atque laudantium non non est.",
                Name = "est",
                TeamId = 2
            };
            (_fakeRepo as FakeGenericRepository<Project>)._repo.Add(project);

        }

        [TearDown]
        public void TestTearDown()
        {
            (_fakeRepo as FakeGenericRepository<Project>)._repo.Clear();
        }
        [Test]
        public async Task GetById_When_ID_0_Then_Received_Object_With_Id_0()
        {
            //Arrange
            var projectService = new ProjectServiceAsyncWithMapping(_fakeUow, _mapper, _fakeQService);
            //Act
            var project = await projectService.GetAsync(0);
            //Assert
            Assert.That(project.Id, Is.EqualTo(0));
        }

        [Test]
        public void SendMessageToQueue_When_Call_Then_Call_QueueService_PushMessage()
        {

            var map = A.Fake<IMapper>();
            var fuow = A.Fake<IUnitOfWorkAsync>();
            var qs = A.Fake<IQueueService>();
            var rps = A.Fake<IRepositoryAsync<Project>>();
            var projSrvc = new ProjectServiceAsyncWithMapping(fuow, map, qs);

            projSrvc.SendMessageToQueue("Hello");
            A.CallTo(() => qs.PushMessage(A<string>._)).MustHaveHappened();

        }

        [Test]
        public async Task GetAsyncMapped_When_Call_Then_ProjectRepositoryCalled()
        {
            var map = A.Fake<IMapper>();
            var fuow = A.Fake<IUnitOfWorkAsync>();
            var qs = A.Fake<IQueueService>();
            var rps = A.Fake<IRepositoryAsync<Project>>();
            var projSrvc = new ProjectServiceAsyncWithMapping(fuow, map, qs);

            var projectdto = await projSrvc.GetAsyncMapped(0);
            A.CallTo(() => fuow.ProjectRepository).MustHaveHappened();
        }

        [Test]
        public async Task GetAsyncMapped_When_Call_Then_Call_MappingService()
        {
            var map = A.Fake<IMapper>();
            var fuow = A.Fake<IUnitOfWorkAsync>();
            var qs = A.Fake<IQueueService>();
            var projSrvc = new ProjectServiceAsyncWithMapping(fuow, map, qs);

            var projectdto = await projSrvc.GetAsyncMapped(0);

            A.CallTo(() => map.Map<ProjectDTO>(A<Project>._)).MustHaveHappened();
        }

        [Test]
        public async Task GetAllAsyncMapped_When_Call_Return_One_ProjectDTO_In_Collection()
        {
            var projectService = new ProjectServiceAsyncWithMapping(_fakeUow, _mapper, _fakeQService);

            var projects = await projectService.GetAllAsyncMapped();

            Assert.That((projects as List<ProjectDTO>).Count, Is.EqualTo(1));
        }

        [Test]
        public async Task CreateAsyncMapped_When_Calle_Then_Call_Repository_InsertAsync()
        {
            var map = A.Fake<IMapper>();
            var fuow = A.Fake<IUnitOfWorkAsync>();
            var qs = A.Fake<IQueueService>();
            var rps = A.Fake<IRepositoryAsync<Project>>();
            var fProject = A.Fake<Project>();
            var fCreatProjectDto = A.Fake<CreatProjectDTO>();
            var projSrvc = new ProjectServiceAsyncWithMapping(fuow, map, qs);

            A.CallTo(() => fuow.ProjectRepository).Returns(rps);
            A.CallTo(() => rps.InsertAsync(A<Project>.Ignored)).Returns(Task.FromResult(fProject));

            var savedProject = await projSrvc.CreateAsyncMapped(fCreatProjectDto);

            A.CallTo(() => rps.InsertAsync(A<Project>.Ignored)).MustHaveHappened();

        }

        [Test]
        public async Task UpdateAsyncMapped_When_Call_Then_Call_RepositoryUpdateAsync()
        {
            var map = A.Fake<IMapper>();
            var fuow = A.Fake<IUnitOfWorkAsync>();
            var qs = A.Fake<IQueueService>();
            var rps = A.Fake<IRepositoryAsync<Project>>();
            var fProject = A.Fake<Project>();
            var fProjectDto = A.Fake<ProjectDTO>();
            var projSrvc = new ProjectServiceAsyncWithMapping(fuow, map, qs);

            A.CallTo(() => fuow.ProjectRepository).Returns(rps);

            await projSrvc.UpdateAsyncMapped(fProjectDto);

            A.CallTo(() => rps.UpdateAsync(A<int>.Ignored,A<Project>.Ignored)).MustHaveHappened();

        }

        [Test]
        public async Task UpdateAsyncMapped_When_Call_Then_Call_SaveChanges()
        {
            var map = A.Fake<IMapper>();
            var fuow = A.Fake<IUnitOfWorkAsync>();
            var qs = A.Fake<IQueueService>();
            var fProjectDto = A.Fake<ProjectDTO>();
            var projSrvc = new ProjectServiceAsyncWithMapping(fuow, map, qs);

            await projSrvc.UpdateAsyncMapped(fProjectDto);

            A.CallTo(() => fuow.SaveChangesAsync()).MustHaveHappened();
        }

        [Test]
        public void UpdateAsyncMapped_When_No_Entity_Then_Throw_Exception()
        {
            var map = A.Fake<IMapper>();
            var fuow = A.Fake<IUnitOfWorkAsync>();
            var qs = A.Fake<IQueueService>();
            var rps = A.Fake<IRepositoryAsync<Project>>();
            var fProject = A.Fake<Project>();
            var fProjectDto = A.Fake<ProjectDTO>();
            var projSrvc = new ProjectServiceAsyncWithMapping(fuow, map, qs);

            A.CallTo(() => fuow.ProjectRepository).Returns(rps);
            A.CallTo(() => rps.GetByIdAsync(A<int>._)).Returns(Task.FromResult(default(Project)));

            Assert.ThrowsAsync<Exception>(async () => await projSrvc.UpdateAsyncMapped(fProjectDto));

        }

        [Test]
        public async Task DeleteAsync_When_Call_Then_Call_RepositoryDeleteAsync()
        {
            var map = A.Fake<IMapper>();
            var fuow = A.Fake<IUnitOfWorkAsync>();
            var qs = A.Fake<IQueueService>();
            var rps = A.Fake<IRepositoryAsync<Project>>();
            var projSrvc = new ProjectServiceAsyncWithMapping(fuow, map, qs);
            A.CallTo(() => fuow.ProjectRepository).Returns(rps);

            await projSrvc.DeleteAsync(0);
            A.CallTo(() => rps.DeleteAsync(A<int>._)).MustHaveHappened();
        }



    }
}
