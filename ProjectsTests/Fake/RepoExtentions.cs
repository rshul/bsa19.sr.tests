﻿using ProjectStructure.Common.Interfaces;
using ProjectStructure.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace ProjectsTests.Fake
{
    public static class RepoExtentions
    {
        private static readonly Dictionary<Type, int> addCounter = new Dictionary<Type, int>()
        {
            [typeof(Project)] = 0,
            [typeof(ProjectTask)] = 0,
            [typeof(User)] = 0,
            [typeof(Team)] = 0,
            [typeof(TaskState)] = 0
        };

        public static int AutoIncAdd<T>(this List<T> repo, T element) where T : class, IEntity
        {
            element.Id = addCounter[typeof(T)]++;
            repo.Add(element);
            return element.Id;
        }
    }
}
