﻿using ProjectStructure.DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ProjectsTests.Fake
{
    public class FakeContext : IDisposable, IChangesTrackableAsync
    {
        public void Dispose()
        {

        }

        public List<T> FakeEntities<T>() => new List<T>();

        public async Task<int> SaveChangesAsync()
        {
            await Task.Yield();
            return 1;
        }
    }
}
