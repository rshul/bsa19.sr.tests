﻿using ProjectStructure.DAL.Entities;
using ProjectStructure.DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ProjectsTests.Fake
{
    public class FakeUnitOfWork : IUnitOfWorkAsync
    {

        private readonly FakeContext _context;

        private IRepositoryAsync<Project> _projectRepository;

        private IRepositoryAsync<User> _userRepository;

        private IRepositoryAsync<Team> _teamRepository;

        private IRepositoryAsync<TaskState> _taskStateRepository;

        private IRepositoryAsync<ProjectTask> _projectTaskRepository;

        public FakeUnitOfWork(FakeContext context)
        {
            _context = context;
        }

        public IRepositoryAsync<Project> ProjectRepository
        {
            get
            {
                if (_projectRepository == null)
                {
                    _projectRepository = new FakeGenericRepository<Project>(_context);
                }
                return _projectRepository;
            }
        }

        public IRepositoryAsync<User> UserRepository
        {
            get
            {
                if (_userRepository == null)
                {
                    _userRepository = new FakeGenericRepository<User>(_context);
                }
                return _userRepository;
            }
        }

        public IRepositoryAsync<Team> TeamRepository
        {
            get
            {
                if (_teamRepository == null)
                {
                    _teamRepository = new FakeGenericRepository<Team>(_context);
                }
                return _teamRepository;
            }
        }


        public IRepositoryAsync<TaskState> TaskStateRepository
        {
            get
            {
                if (_taskStateRepository == null)
                {
                    _taskStateRepository = new FakeGenericRepository<TaskState>(_context);
                }
                return _taskStateRepository;
            }
        }



        public IRepositoryAsync<ProjectTask> ProjectTaskRepository
        {
            get
            {
                if (_projectTaskRepository == null)
                {
                    _projectTaskRepository = new FakeGenericRepository<ProjectTask>(_context);
                }
                return _projectTaskRepository;
            }
        }



        public void Dispose()
        {
            _context?.Dispose();
        }

        public async Task<int> SaveChangesAsync()
        {

            return await _context.SaveChangesAsync();
        }
    }
}
