﻿using ProjectStructure.Common.Interfaces;
using ProjectStructure.DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ProjectsTests.Fake
{
    class FakeGenericRepository<TEntity> : IRepositoryAsync<TEntity> where TEntity : class, IEntity
    {

        public  List<TEntity> _repo;

        public FakeGenericRepository(FakeContext context)
        {
            _repo = context.FakeEntities<TEntity>();

        }

        public async Task DeleteAsync(int id)
        {
            var elementToDelete = await GetByIdAsync(id);
            _repo.Remove(elementToDelete);
        }

        public async Task<IEnumerable<TEntity>> GetAllAsync()
        {
            
            return await Task.Run(() => _repo);
        }

        public async Task<TEntity> GetByIdAsync(int id)
        {
            return await Task.Run(() => _repo.Find(e => e.Id == id));

        }

        public async Task<TEntity> InsertAsync(TEntity entity)
        {
            var newId = Task.Run(() => _repo.AutoIncAdd(entity));
            entity.Id = await newId;
            return entity;
        }

        public async Task UpdateAsync(int id, TEntity entity)
        {
            var elementToChange = await GetByIdAsync(id);
            elementToChange = entity;
        }
    }
}
