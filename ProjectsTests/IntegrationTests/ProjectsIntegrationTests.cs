﻿using AutoMapper;
using Microsoft.Extensions.Configuration;
using NUnit.Framework;
using ProjectsTests.Fake;
using ProjectStructure.BLL.Interfaces;
using ProjectStructure.BLL.MappingProfiles;
using ProjectStructure.BLL.Services;
using ProjectStructure.Common;
using ProjectStructure.DAL.Entities;
using ProjectStructure.DAL.Interfaces;
using ProjectStructureWebApi.Controllers;
using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;

using System.Text;
using System.Net;
using System.Threading.Tasks;
using System.Linq;
using System.Net.Http;

namespace ProjectsTests.IntegrationTests
{
    [TestFixture]
    public class ProjectsIntegrationTests
    {
        private readonly IQueueService _fakeQService;
        private readonly IMapper _mapper;
        private readonly IUnitOfWorkAsync _fakeUow;
        private readonly IRepositoryAsync<Project> _fakeRepoProject;
        private readonly IRepositoryAsync<ProjectTask> _fakeRepoTask;
        private readonly IRepositoryAsync<Team> _fakeRepoTeam;
        private readonly IRepositoryAsync<User> _fakeRepoUser;
        private readonly IServiceAsyncWithMapping<Project, ProjectDTO, CreatProjectDTO> _projectService;
        private readonly IServiceAsyncWithMapping<User, UserDTO, CreateUserDTO> _userService;
        private readonly IServiceAsyncWithMapping<ProjectTask, ProjectTaskDTO, CreateProjectTaskDTO> _taskService;
        private readonly IServiceAsyncWithMapping<Team, TeamDTO, CreateTeamDTO> _teamService;
        private readonly ProjectsController _projectsController;
        private readonly TasksController _tasksController;
        private readonly TeamsController _teamsController;
        private readonly UsersController _usersController;
        private CreatProjectDTO createProjectDto;
        private CreateTeamDTO createTeamDto;
        private ProjectTaskDTO taskDto;

        public ProjectsIntegrationTests()
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile<ProjectProfile>();
                cfg.AddProfile<TeamProfile>();
            });
            var mapper = config.CreateMapper();
            _mapper = mapper;
            var builder = new ConfigurationBuilder()
            .AddJsonFile("appSettings.json");
            IConfiguration configuration = builder.Build();

            _fakeUow = new FakeUnitOfWork(new FakeContext());
            _fakeRepoProject = _fakeUow.ProjectRepository;
            _fakeRepoTask = _fakeUow.ProjectTaskRepository;
            _fakeRepoTeam = _fakeUow.TeamRepository;
            _fakeRepoUser = _fakeUow.UserRepository;
            _fakeQService = new FakeQueService();
            _projectService = new ProjectServiceAsyncWithMapping(_fakeUow, mapper, new FakeQueService());
            _taskService = new TaskServiceAsyncWithMapping(_fakeUow, mapper, new FakeQueService());
            _teamService = new TeamServiceAsyncWithMapping(_fakeUow, mapper, new FakeQueService());
            _userService = new UserServiceAsyncWithMapping(_fakeUow, mapper, new FakeQueService());
            _projectsController = new ProjectsController(_projectService, configuration);
            _tasksController = new TasksController(_taskService);
            _teamsController = new TeamsController(_teamService);
            _usersController = new UsersController(_userService);

        }

        [SetUp]
        public async Task Setup()
        {
            await _fakeRepoProject.InsertAsync(new Project
            {
                Id = 1,
                AuthorId = 2,
                CreatedAt = new DateTime(2017, 2, 19, 21, 53, 20, 150, DateTimeKind.Local).AddTicks(8055),
                Deadline = new DateTime(2020, 9, 19, 12, 36, 3, 3, DateTimeKind.Local).AddTicks(1465),
                Description = @"Laborum nihil atque itaque ea sed quisquam dicta et eum.
Quisquam maxime consequatur quaerat autem in.",
                Name = "autem",
                TeamId = 1
            });

            await _fakeRepoTask.InsertAsync(new ProjectTask
            {
                Id = 9,
                CreatedAt = new DateTime(2014, 11, 24, 22, 8, 30, 652, DateTimeKind.Local).AddTicks(9839),
                Description = @"Ea dolor aut incidunt asperiores laborum id.
Ex quia vero ut amet quibusdam saepe.
Laboriosam incidunt cumque.
Labore inventore sint deserunt veniam saepe.
Possimus et nulla qui officia quia et provident pariatur.",
                FinishedAt = new DateTime(2019, 10, 1, 5, 50, 32, 118, DateTimeKind.Local).AddTicks(7290),
                Name = "quis",
                PerformerId = 14,
                PojectId = 1,
                TaskStateId = 3
            });

            await _fakeRepoUser.InsertAsync(new User
            {
                Id = 14,
                Birthday = new DateTime(2019, 5, 24, 6, 11, 24, 208, DateTimeKind.Local).AddTicks(8968),
                Email = "Ransom3@yahoo.com",
                FirstName = "Hermina",
                LastName = "Weissnat",
                RegisteredAt = new DateTime(2019, 5, 26, 23, 38, 15, 114, DateTimeKind.Local).AddTicks(9488),
                TeamId = 1
            });

            await _fakeRepoTeam.InsertAsync(new Team
            {
                Id = 1,
                CreatedAt = new DateTime(2019, 7, 15, 21, 2, 14, 784, DateTimeKind.Local).AddTicks(7685),
                Name = "iure"
            });

            createProjectDto = new CreatProjectDTO
            {
                Author_Id = 7,
                Created_At = new DateTime(2019, 2, 22, 8, 53, 46, 371, DateTimeKind.Local).AddTicks(846),
                Deadline = new DateTime(2021, 7, 14, 14, 26, 27, 380, DateTimeKind.Local).AddTicks(6291),
                Description = @"Ut sapiente neque eos debitis.
Dolorum laborum soluta vitae.
Aut et sed voluptatem quisquam repellendus repellat dicta.
Ullam perspiciatis neque et.",
                Name = "tempore",
                Team_Id = 3
            };

            createTeamDto = new CreateTeamDTO
            {
                Created_At = new DateTime(2019, 7, 15, 22, 26, 1, 153, DateTimeKind.Local).AddTicks(9200),
                Name = "deleniti"
            };
            taskDto = new ProjectTaskDTO
            {
                Id = 9,
                Created_At = new DateTime(2014, 11, 24, 22, 8, 30, 652, DateTimeKind.Local).AddTicks(9839),
                Description = @"Ea dolor aut incidunt asperiores laborum id.
Ex quia vero ut amet quibusdam saepe.
Laboriosam incidunt cumque.
Labore inventore sint deserunt veniam saepe.
Possimus et nulla qui officia quia et provident pariatur.",
                Finished_At = new DateTime(2019, 10, 1, 5, 50, 32, 118, DateTimeKind.Local).AddTicks(7290),
                Name = "quis",
                Performer_Id = 14,
                Project_Id = 1,
                State = 3

            };

        }

        [TearDown]
        public void TestTearDown()
        {
            (_fakeRepoProject as FakeGenericRepository<Project>)?._repo.Clear();
            (_fakeRepoTask as FakeGenericRepository<ProjectTask>)?._repo.Clear();
            (_fakeRepoUser as FakeGenericRepository<User>)?._repo.Clear();
            (_fakeRepoTeam as FakeGenericRepository<Team>)?._repo.Clear();
        }

        [Test]
        public async Task When_Create_Project_Then_Project_Saved_In_Database()
        {
            ObjectResult result = await _projectsController.Post(createProjectDto) as ObjectResult;
            var projects = await _fakeRepoProject.GetAllAsync();
            projects = projects.ToList();

            Assert.That(result.StatusCode, Is.EqualTo((int)HttpStatusCode.OK));
            Assert.That(projects.Count, Is.EqualTo(2));
            StringAssert.Contains("tempore", projects.LastOrDefault().Name);

        }

        [Test]
        public async Task When_Create_Team_Then_Team_Saved_In_Database()
        {
            ObjectResult result = await _teamsController.Post(createTeamDto) as ObjectResult;
            var teams = await _fakeRepoTeam.GetAllAsync();
            teams = teams.ToList();

            Assert.That(result.StatusCode, Is.EqualTo((int)HttpStatusCode.OK));
            Assert.That(teams.Count, Is.EqualTo(2));
            StringAssert.Contains("deleniti", teams.LastOrDefault().Name);

        }

        [Test]
        public async Task When_Delete_User_Then_User_Deleted_From_Database()
        {
           var result = await _usersController.Delete(0) as StatusCodeResult;
           Assert.That(result.StatusCode, Is.AnyOf(200, 204));
        }



        [Test]
        public async Task When_Delete_Task_Then_Task_Deleted_From_Database()
        {
            var result = await _tasksController.Delete(0);
           
            var statusCode = (result as StatusCodeResult).StatusCode;
            

            Assert.That(statusCode, Is.AnyOf(200, 204));
            
        }



    }
}
