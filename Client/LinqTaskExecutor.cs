﻿using Client.EntityData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Client
{
    public class LinqTaskExecutor
    {
        private readonly DataRepository _dataSource;

        private IEnumerable<ProjectData> _cachedProjects;

        public IEnumerable<ProjectData> CachedProjects
        {
            get
            {
                if (_cachedProjects == null)
                {
                    return _cachedProjects = _dataSource.GetAllProjectDataAsync().Result;
                }
                else
                {
                    return _cachedProjects;
                }

            }
        }

        public LinqTaskExecutor()
        {
            _dataSource = new DataRepository();
        }

        public IDictionary<int, int> T1_GetNumberTasksInProjectOfUser(int id)
        {
            return CachedProjects
                .Where(p => p.TasksDatas.Any(t => t.Performer.Id == id))
                .Select(p => (projId: p.Id, num: p.TasksDatas.Count))
                .ToDictionary(e => e.projId, e => e.num);

        }

        public IEnumerable<TaskData> T2_GetTasksForUser(int id)
        {
            return CachedProjects
                .SelectMany(p => p.TasksDatas)
                .GroupBy(t => t.Id)
                .Select(tg => tg.FirstOrDefault())
                .Where(t => t.Performer.Id == id && t.Name.Length < 45);

        }

        public IEnumerable<(int, string)> T3_GetFinishedTasksForUser(int id)
        {
            return CachedProjects
                .SelectMany(p => p.TasksDatas)
                .GroupBy(t => t.Id)
                .Select(tg => tg.FirstOrDefault())
                .Where(t => t.Performer.Id == id)
                .Where(t => t.FinishedAt.Year == 2019 && t.State == "Finished")
                .Select(t => (t.Id, t.Name));

        }

        public IEnumerable<(int, string, List<UserData>)> T4_GetTeamsWithSortedUsers()
        {
            return CachedProjects
                .SelectMany(p => p.TasksDatas)
                .Select(t => t.Performer)
                .GroupBy(u => u.Id)
                .Select(u => u.FirstOrDefault())
                .Where(u => u.Birthday.Year > 12 && u.TeamId != null)
                .OrderByDescending(u => u.RegisteredAt)
                .GroupBy(u => u.TeamId)
                .Join(CachedProjects
                        .GroupBy(p => p.Team.Id)
                        .Select(p => p.FirstOrDefault().Team), uGr => uGr.Key, t => t.Id, (uGr, t) => (uGr.Key.Value, t.Name, uGr.ToList()));
        }

        public IEnumerable<(UserData, List<TaskData>)> T5_GetSortedUsersWithSortedTasks()
        {
            return CachedProjects
                .SelectMany(p => p.TasksDatas)
                .GroupBy(t => t.Id)
                .Select(tg => tg.FirstOrDefault())
                .GroupBy(t => t.Performer.Id,
                         t => t,
                        (uId, tg) => (user: tg.FirstOrDefault().Performer, tasks: tg.OrderByDescending(t => t.Name).ToList()))
                .OrderBy(ls => ls.user.FirstName);
        }

        public (UserData user, ProjectData lastProject, int? lastProjectTasksNumber, int? notCompletedTasksNumber, TaskData longestTask) T6_GetUserInfo(int id)
        {

            var lastProjectOfUser = CachedProjects
                .Where(p => p.Author.Id == id)
                .OrderBy(p => p.CreatedAt)
                .LastOrDefault();

            var projectTasksNumber = lastProjectOfUser?.TasksDatas.Count();

            var usersTasks = CachedProjects
                .SelectMany(p => p.TasksDatas)
                .GroupBy(t => t.Id)
                .Select(tg => tg.FirstOrDefault())
                .Where(t => t.Performer.Id == id);

            var numberNotCompletedTasks = usersTasks.Where(t => t.State != "Finished").Count();
            var longestTask = usersTasks.OrderBy(t => t.FinishedAt - t.CreatedAt).LastOrDefault();
            var user = lastProjectOfUser?.Author ?? usersTasks?.FirstOrDefault()?.Performer;

            return (user, lastProjectOfUser, projectTasksNumber, numberNotCompletedTasks, longestTask);
        }

        public (ProjectData project, TaskData longest, TaskData shortest, int usersNumber) T7_GetProjectInfo(int id)
        {
            return CachedProjects
                .Where(p => p.Id == id)
                .Select(p => (project: p,
                            longestTask: p.TasksDatas.OrderBy(t => t.Description.Length).LastOrDefault(),
                            shortestTask: p.TasksDatas.OrderBy(t => t.Name.Length).FirstOrDefault(),
                            usersInProjects: CachedProjects
                                                .Where(pr => pr.Description.Length > 25 || pr.TasksDatas.Count() < 3)
                                                .Select(pr => pr.Team.Id).Distinct()
                                                .GroupJoin(CachedProjects
                                                                .SelectMany(pr => pr.TasksDatas)
                                                                .GroupBy(t => t.Id)
                                                                .Select(tg => tg.FirstOrDefault())
                                                                .Select(t => t.Performer)
                                                                .GroupBy(u => u.Id)
                                                                .Select(ug => ug.FirstOrDefault()),
                                                            t => t,
                                                            u => u.TeamId,
                                                            (t, us) => us.Count()).Sum())).FirstOrDefault();
        }
    }
}
