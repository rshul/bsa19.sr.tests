﻿using Client.ApiTesters;
using Client.Repositories;
using Newtonsoft.Json;
using ProjectStructure.Common;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Client
{
    public class StartupMain
    {
        public static async Task Run()
        {
            await MarkRandomTaskExample();
            //await TestProjectsAPI();
            
        }

        public static async Task TestProjectsAPI()
        {
            var repo = new ProjectsClientRepo();
            var pt = new ProjectsApiTester(repo);
            //pt.GenerateProjects(10);
            //pt.SendProjects();
            IEnumerable<ProjectDTO> prs = await pt.GetAllProjects();
            var c = JsonConvert.SerializeObject(prs, Formatting.Indented);
            Console.WriteLine(c);
            var project = await pt.GetOneProject((prs as List<ProjectDTO>)[0].Id);
            project.Name = "Upd";
            await pt.UpdateProject(project);
            //pt.DeleteProject(2).GetAwaiter();
            IEnumerable<ProjectDTO> prsa = await pt.GetAllProjects();
            var ca = JsonConvert.SerializeObject(prsa, Formatting.Indented);
            Console.WriteLine(ca);
            await Task.Delay(10);
        }
        public static async Task MarkRandomTaskExample()
        {
            TaskMarker queries = new TaskMarker(new TasksRepo());

            Task<int> markedTaskId = queries.MarkRandomTaskWithDelay(1000);
            // some work here
            Console.WriteLine("Not task work");

            try
            {
                Console.WriteLine($"Printed result: {await markedTaskId}");
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }
        public static void ShowLinkExamples()
        {
            var lte = new LinqTasksExemples(new LinqTaskExecutor());
            lte.ShowExemples();
        }
    }
}
