﻿using ProjectStructure.Common;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Client.Repositories
{
    public class UsersRepo: IRepo<UserDTO>
    {
        private string path = "Users";

        public UsersRepo()
        {
            HttpService.SetBaseUrl(@"https://localhost:5001/api/");
        }
        public async Task Delete(int id)
        {
            await HttpService.DeleteObjectAsync(id, path);
        }

        public async Task<IEnumerable<UserDTO>> GetAll()
        {
            return await HttpService.GetObjectsAsync<UserDTO>(path);
        }

        public async Task<UserDTO> GetById(int id)
        {
            return await HttpService.GetObjecttAsync<UserDTO>(id, path);
        }

        public async Task Insert(UserDTO entity)
        {
            await HttpService.CreateProductAsync<UserDTO>(entity, path);
        }

        public async Task<bool> Update( UserDTO entity)
        {
            return await HttpService.UpdateObjectAsync<UserDTO>( entity, path);
        }
    }
}
