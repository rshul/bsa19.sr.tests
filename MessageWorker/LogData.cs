﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MessageWorker
{
    public class LogData
    {
        public string Message { get; set; }
        public DateTime MessageDateTime { get; set; }
    }
}
