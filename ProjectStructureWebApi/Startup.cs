﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using ProjectStructure.BLL.Hubs;
using ProjectStructure.BLL.Interfaces;
using ProjectStructure.BLL.Services;
using ProjectStructure.Common;
using ProjectStructure.DAL;
using ProjectStructure.DAL.Context;
using ProjectStructure.DAL.Interfaces;
using Microsoft.EntityFrameworkCore;
using ProjectStructure.DAL.Entities;
using ProjectStructureWebApi.Extentions;

namespace ProjectStructureWebApi
{
    public class Startup
    {
        public Startup(IHostingEnvironment env)
        {
            var builder = new ConfigurationBuilder()
            .SetBasePath(env.ContentRootPath)
            .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true);
        Configuration = builder.Build();
            
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);
            services.AddDbContext<ProjectDbContext>(options => options.UseSqlServer(Configuration.GetConnectionString("ProjectsDbConnection")));
            services.AddScoped<IUnitOfWorkAsync, UnitOfWorkEf>();
        
            services.AddScoped<IServiceAsyncWithMapping<Project, ProjectDTO, CreatProjectDTO>, ProjectServiceAsyncWithMapping>();
            services.AddScoped<IServiceAsyncWithMapping<ProjectTask, ProjectTaskDTO, CreateProjectTaskDTO>, TaskServiceAsyncWithMapping>();
            services.AddScoped<IServiceAsyncWithMapping<TaskState,TaskStateDTO, CreateTaskStateDTO>, TaskStateServiceAsyncWithMapping>();
            services.AddScoped<IServiceAsyncWithMapping<Team,TeamDTO,CreateTeamDTO>, TeamServiceAsyncWithMapping>();
            services.AddScoped<IServiceAsyncWithMapping<User,UserDTO,CreateUserDTO>, UserServiceAsyncWithMapping>();
            services.AddScoped<IQueueService, QueueServiceBeta>();
            services.RegisterAutomapper();
            
            services.AddCors();
            services.AddSignalR();

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }
            app.UseCors(builder => builder
            .AllowAnyMethod()
            .AllowAnyHeader()
            .WithExposedHeaders("Token-Expired")
            .AllowCredentials()
            .WithOrigins("http://localhost:4200"));
            
            app.UseSignalR(routes => 
            {
                routes.MapHub<MessageHub>("/messages");
            });
            app.UseHttpsRedirection();
            app.UseMvc();
        }
    }
}
