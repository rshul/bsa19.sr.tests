using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using ProjectStructure.BLL.Interfaces;
using ProjectStructure.BLL.Services;
using ProjectStructure.Common;
using ProjectStructure.DAL.Entities;

namespace ProjectStructureWebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UsersController : ControllerBase
    {
        private IServiceAsyncWithMapping<User,UserDTO,CreateUserDTO> _userService;
       
        public UsersController(IServiceAsyncWithMapping<User, UserDTO, CreateUserDTO> userService)
        {
            _userService = userService;
         
        }

        // GET api/values
        [HttpGet]
        public async Task<ActionResult> GetAll()
        {
            var messageSource = (
              classSource: this.GetType().Name,
              methodSource: new StackFrame(2).GetMethod().Name);
            _userService.SendMessageToQueue($"{messageSource.classSource}.{messageSource.methodSource} was triggered ");

            var collection = await _userService.GetAllAsyncMapped();
            return Ok(collection);
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public async Task<ActionResult> Get(int id)
        {
            var messageSource = (
              classSource: this.GetType().Name,
              methodSource: new StackFrame(2).GetMethod().Name);
            _userService.SendMessageToQueue($"{messageSource.classSource}.{messageSource.methodSource} was triggered ");

            UserDTO foundEntity;
            try
            {
                foundEntity = await _userService.GetAsyncMapped(id);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
            return Ok(foundEntity);
        }

        // POST api/values
        [HttpPost]
        public async Task<ActionResult> Post([FromBody] CreateUserDTO value)
        {
            var messageSource = (
              classSource: this.GetType().Name,
              methodSource: new StackFrame(2).GetMethod().Name);
            _userService.SendMessageToQueue($"{messageSource.classSource}.{messageSource.methodSource} was triggered ");

            var result = await _userService.CreateAsyncMapped(value);
            return Ok(result);
        }

        // PUT api/values/5
        [HttpPut]
        public async Task<ActionResult> Put([FromBody] UserDTO value)
        {
            var messageSource = (
              classSource: this.GetType().Name,
              methodSource: new StackFrame(2).GetMethod().Name);
            _userService.SendMessageToQueue($"{messageSource.classSource}.{messageSource.methodSource} was triggered ");

            try
            {
                await _userService.UpdateAsyncMapped(value);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
            return NoContent();
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public async Task<ActionResult> Delete(int id)
        {
            var messageSource = (
              classSource: this.GetType().Name,
              methodSource: new StackFrame(2).GetMethod().Name);
            _userService.SendMessageToQueue($"{messageSource.classSource}.{messageSource.methodSource} was triggered ");

            try
            {
                await _userService.DeleteAsync(id);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
            return NoContent();
        }




    }
}