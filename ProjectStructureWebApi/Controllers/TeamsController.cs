using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using ProjectStructure.BLL.Interfaces;
using ProjectStructure.BLL.Services;
using ProjectStructure.Common;
using ProjectStructure.DAL.Entities;

namespace ProjectStructureWebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TeamsController : ControllerBase
    {
        private IServiceAsyncWithMapping<Team,TeamDTO,CreateTeamDTO> _teamService;

        public TeamsController(IServiceAsyncWithMapping<Team, TeamDTO, CreateTeamDTO> teamService)
        {
            _teamService = teamService;
        }

        // GET api/values
        [HttpGet]
        public async Task<IActionResult> GetAll()
        {
            var messageSource = (
              classSource: this.GetType().Name,
              methodSource: new StackFrame(2).GetMethod().Name);
            _teamService.SendMessageToQueue($"{messageSource.classSource}.{messageSource.methodSource} was triggered ");

            var collection = await _teamService.GetAllAsyncMapped();
            return Ok(collection);
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public async Task<ActionResult> Get(int id)
        {
            var messageSource = (
               classSource: this.GetType().Name,
               methodSource: new StackFrame(2).GetMethod().Name);
            _teamService.SendMessageToQueue($"{messageSource.classSource}.{messageSource.methodSource} was triggered ");

            TeamDTO foundEntity;
            try
            {
                foundEntity = await _teamService.GetAsyncMapped(id);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
            return Ok(foundEntity);
        }

        // POST api/values
        [HttpPost]
        public async Task<IActionResult> Post([FromBody] CreateTeamDTO value)
        {
            var messageSource = (
              classSource: this.GetType().Name,
              methodSource: new StackFrame(2).GetMethod().Name);
            _teamService.SendMessageToQueue($"{messageSource.classSource}.{messageSource.methodSource} was triggered ");

            var result = await _teamService.CreateAsyncMapped(value);
            return Ok(result);
        }

        // PUT api/values/5
        [HttpPut]
        public async Task<ActionResult> Put([FromBody] TeamDTO value)
        {
            var messageSource = (
              classSource: this.GetType().Name,
              methodSource: new StackFrame(2).GetMethod().Name);
            _teamService.SendMessageToQueue($"{messageSource.classSource}.{messageSource.methodSource} was triggered ");

            try
            {
                await _teamService.UpdateAsyncMapped(value);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
            return NoContent();

        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public async Task<ActionResult> Delete(int id)
        {

            var messageSource = (
              classSource: this.GetType().Name,
              methodSource: new StackFrame(2).GetMethod().Name);
            _teamService.SendMessageToQueue($"{messageSource.classSource}.{messageSource.methodSource} was triggered ");

            try
            {
                await _teamService.DeleteAsync(id);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
            return NoContent();
        }



    }
}